<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%
String root = "resources/statics";
String view = "cdk.app";

 %>

<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Expires" content="1" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="No-Cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>CDK-Market</title>
	
	<%-- Style Sheet --%>
    <link rel="stylesheet" type="text/css" href="<%=root%>/css/all.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>/css/icons.css" />
    <%-- //Style Sheet --%>
	
    <script type="text/javascript" src="<%=root%>/js/lib/jquery/jquery-2.1.1.min.js" charset="utf-8"></script>
	
    <%-- ExtJS 4.2.1 --%>
    <script type="text/javascript" src="<%=root%>/js/lib/ext-4.2.1/bootstrap.js" charset="utf-8"></script>
    <script type="text/javascript" src="<%=root%>/js/lib/ext-4.2.1/ext-theme-neptune.js" charset="utf-8"></script>
    <script type="text/javascript" src="<%=root%>/js/lib/ext-4.2.1/locale/ext-lang-ko.js" charset="utf-8"></script>
    <%-- //ExtJS 4.2.1 --%>
    
    <%-- ExtJS 4.2.1 Bugfix --%>
    <script type="text/javascript" src="<%=root%>/js/lib/patches/patch-ext-view-table.js" charset="utf-8"></script>
    <script type="text/javascript" src="<%=root%>/js/lib/patches/patch-grouping-summary.js" charset="utf-8"></script>
    <script type="text/javascript" src="<%=root%>/js/lib/patches/patch-roweditor-quicktip.js" charset="utf-8"></script>
    <%-- //ExtJS 4.2.1 Bugfix --%>
    
    <%-- //Custom --%>
    <script type="text/javascript" src="<%=root%>/js/cdk/util.js" charset="utf-8"></script>
    <%-- //Custom --%>
    
    <script type="text/javascript" charset="utf-8">
    
    Ext.Loader.setConfig({
        enabled: true,
        paths: {
            'Ext.ux': './<%=root%>/js/lib/ext-4.2.1/ux',
            'Ext.ux.form.field': './<%=root%>/js/lib/ext-4.2.1/ux/form/field',
            'cdk': './<%=root%>/js/cdk'
        }
    });
    
    Ext.BLANK_IMAGE_URL = './<%=root%>/images/s.gif';
    
    Ext.QuickTips.init();
    
    // Tooltip 초기화
    Ext.apply(Ext.QuickTips.getQuickTip(), {
        showDelay : 250,
        hideDelay : 300,
        dismissDelay : 0
    });
    
    // History 초기화
    Ext.History.init();
    
    Ext.application({
        name: 'CDK',
        launch: function() {
            delete Ext.tip.Tip.prototype.minWidth;
            
            Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
                path: '<%=request.getContextPath()%>',
                expires: null
            }));

            Ext.cdk = Ext.create('${view}');
        }
    });
    
    </script>
    
</head>

<body>
<div id="loading" style="display:none">
    <img src="./<%=root%>/images/loading-image.gif"/>
</div>

<form id="history-form" class="x-hide-display">
    <input type="hidden" id="x-history-field" />
    <iframe id="x-history-frame"></iframe>
</form>
</body>

</html>