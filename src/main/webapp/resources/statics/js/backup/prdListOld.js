Ext.define('cdk.prd.prdListOld', {
    extend: 'Ext.grid.Panel',
    
    requires: [
        'cdk.prd.popup.prdWin'
    ],
    
    title: '제품목록',
    
    initComponent: function() {
        var me = this;
        
        me.isAvail = true;
        
        me.prdStore = Ext.create('Ext.data.Store', {
            fields: [
                'prdKey',
                'prdCatCd',
                'prdCatNm',
                'prdNm',
                'prdOrgPrice',
                'prdPrice',
                'prdPer',
                'prdCnt',
                'prdOldCnt',
                'prdImg',
                'creator',
                'availSt',
                'availEnd'
            ],
            proxy: {
                type: 'ajax',
                url: 'admin/product',
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        Ext.apply(me, {
            dockedItems: [{
                dock: 'top',
                xtype: 'panel',
                bodyPadding: 5,
                margin: '0 10 0 0',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [{
                    xtype: 'button',
                    text: '추가',
                    width: 100,
                    scope: me,
                    handler: me.fnShowAdd
                },{
                    xtype: 'button',
                    text: '수정',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnShowMod
                },{
                    xtype: 'button',
                    text: '삭제',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnRemove
                }]
            }],
            
            store: me.prdStore,
            
            columns: {
                defaults: {
                    style: 'text-align:center',
                    align: 'center',
                    flex: 1
                },
                
                items: [{
                    text: '이미지',
                    dataIndex: 'prdImg'
                },{
                    text: '카테고리',
                    dataIndex: 'prdCatNm'
                },{
                    text: '제품명',
                    dataIndex: 'prdNm'
                },{
                    text: '재고',
                    dataIndex: 'prdCnt'
                },{
                    text: '판매가',
                    dataIndex: 'prdPrice',
                    renderer: Ext.util.Format.numberRenderer('0,000')
                },{
                    text: '원가',
                    dataIndex: 'prdOrgPrice',
                    renderer: Ext.util.Format.numberRenderer('0,000')
                },{
                    text: '마진율',
                    dataIndex: 'prdPer'
                },{
                    text: '이전재고',
                    dataIndex: 'prdOldCnt'
                },{
                    text: '생성자',
                    dataIndex: 'creator'
                },{
                    text: '생성일',
                    dataIndex: 'availSt',
                    renderer: function(v) {
                        return cm.getDateString(v);
                    }
                }]
            },
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    },
    
    fnLoad: function() {
        var me = this;
        me.prdStore.load({
            params: { isAvail: me.isAvail }
        });
    },
    
    fnRemove: function() {
        var me = this,
            selected = me.getSelectionModel().getSelection();
            
        if (!Ext.isEmpty(selected)) {
            Ext.Msg.show({
                msg: '삭제하시겠습니까?',
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.YESNO,
                fn: function(btn) {
                    if (btn === 'yes') {
                        Ext.Ajax.request({
                            url: 'admin/product',
                            method: 'DELETE',
                            jsonData: selected[0].data,
                            success: function(form, action) {
                                me.fnLoad();
                            }
                        });
                    }
                }
            });
        }
    },
    
    fnShowAdd: function() {
        this.showWin();
    },
    
    fnShowMod: function() {
        var selected = this.getSelectionModel().getSelection();
        
        if (!Ext.isEmpty(selected)) {
            this.showWin(selected[0]);
        }
    },
    
    showWin: function(selectedRecord) {
        var me = this;
        
        Ext.widget('productwindow',{
            selectedRecord: selectedRecord,
            callback: function() {
                me.fnLoad();
            }
        }).show();
    }
});