Ext.define('cdk.prd.catListOld', {
    extend: 'Ext.grid.Panel',
    
    requires: [
        'cdk.prd.popup.catWin'
    ],
    
    title: '카테고리 목록',
    
    initComponent: function() {
        var me = this;
        
        me.isAvail = true;
        
        me.catStore = Ext.create('Ext.data.Store', {
            fields: [
                'prdCatCd',
                'prdCatNm',
                'creator',
                'availSt'
            ],
            proxy: {
                type: 'ajax',
                url: 'admin/product/category',
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        Ext.apply(me, {
            dockedItems: [{
                dock: 'top',
                xtype: 'panel',
                bodyPadding: 5,
                margin: '0 10 0 0',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [{
                    xtype: 'button',
                    text: '추가',
                    width: 100,
                    scope: me,
                    handler: me.fnShowAdd
                },{
                    xtype: 'button',
                    text: '수정',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnShowMod
                },{
                    xtype: 'button',
                    text: '삭제',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnRemove
                }]
            }],
            
            store: me.catStore,
            
            columns: {
                defaults: {
                    style: 'text-align:center',
                    align: 'center',
                    flex: 1
                },
                
                items: [{
                    text: '카테고리 코드',
                    dataIndex: 'prdCatCd'
                },{
                    text: '카테고리명',
                    dataIndex: 'prdCatNm'
                },{
                    text: '생성자',
                    dataIndex: 'creator'
                },{
                    text: '생성일',
                    dataIndex: 'availSt',
                    renderer: function(v) {
                        return cm.getDateString(v);
                    }
                }]
            },
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    },
    
    fnLoad: function() {
        var me = this;
        me.catStore.load({
            params: { isAvail: me.isAvail }
        });
    },
    
    fnRemove: function() {
        var me = this,
            selected = me.getSelectionModel().getSelection();
            
        if (!Ext.isEmpty(selected)) {
            Ext.Msg.show({
                msg: '삭제하시겠습니까?',
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.YESNO,
                fn: function(btn) {
                    if (btn === 'yes') {
                        Ext.Ajax.request({
                            url: 'admin/product/category',
                            method: 'DELETE',
                            jsonData: selected[0].data,
                            success: function(form, action) {
                                me.fnLoad();
                            }
                        });
                    }
                }
            });
        }
    },
    
    fnShowAdd: function() {
        this.showWin();
    },
    
    fnShowMod: function() {
        var selected = this.getSelectionModel().getSelection();
        
        if (!Ext.isEmpty(selected)) {
            this.showWin(selected[0]);
        }
    },
    
    showWin: function(selectedRecord) {
        var me = this;
        
        Ext.widget('categorywindow',{
            selectedRecord: selectedRecord,
            callback: function() {
                me.fnLoad();
            }
        }).show();
    }
});