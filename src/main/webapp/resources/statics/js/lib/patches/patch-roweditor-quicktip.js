/**
 * ExtJS 4.2.1 버그픽스
 * 
 * @author jhkang
 * @since 2014.04.11
 */
if (Ext.getVersion().match('4.2.1')) {
	
	/**
	 * Row Editor에서 데이터 입력 후 Update Button이 활성화 되지 않는 문제 해결
	 */
	Ext.override(Ext.grid.RowEditor, {
		addFieldsForColumn: function(column, initial) {
			var me = this, i, length, field;

			if (Ext.isArray(column)) {
				for (i = 0, length = column.length; i < length; i++) {
					me.addFieldsForColumn(column[i], initial);
				}
				return;
			}

			if (column.getEditor) {
				field = column.getEditor(null, {
					xtype : 'displayfield',
					getModelData : function() {
						return null;
					}
				});

				if (column.align === 'right') {
					field.fieldStyle = 'text-align:right';
				}

				if (column.xtype === 'actioncolumn') {
					field.fieldCls += ' ' + Ext.baseCSSPrefix + 'form-action-col-field';
				}

				if (me.isVisible() && me.context) {
					if (field.is('displayfield')) {
						me.renderColumnData(field, me.context.record, column);
					} else {
						field.suspendEvents();
						field.setValue(me.context.record.get(column.dataIndex));
						field.resumeEvents();
					}
				}

				if (column.hidden) {
					me.onColumnHide(column);
				} else if (column.rendered && !initial) {
					me.onColumnShow(column);
				}

				// 버그 수정을 위해 아래 이벤트 추가
				me.mon(field, 'change', me.onFieldChange, me);
			}
		}
	});
	
	/**
	 * 툴팁의 길이가 정상적으로 늘어나지 않는 버그 수정
	 */
	Ext.override(Ext.tip.QuickTip, {
	    helperElId: 'ext-quicktips-tip-helper',
	    initComponent: function() {
	        var me = this;
	 
	        me.target = me.target || Ext.getDoc();
	        me.targets = me.targets || {};
	        me.callParent();
	 
	        // new stuff
	        me.on('move', function() {
	            var offset = me.hasCls('x-tip-form-invalid') ? 35 : 12,
	                helperEl = Ext.fly(me.helperElId) || Ext.fly(
	                    Ext.DomHelper.createDom({
	                        tag: 'div',
	                        id: me.helperElId,
	                        style: {
	                            position: 'absolute',
	                            left: '-1000px',
	                            top: '-1000px',
	                            'font-size': '12px',
	                            'font-family': 'tahoma, arial, verdana, sans-serif'
	                        }
	                    }, Ext.getBody())
	                );
	 
	            if (me.html && (me.html !== helperEl.getHTML() || me.getWidth() !== (helperEl.dom.clientWidth + offset))) {
	                helperEl.update(me.html);
	                me.setWidth(Ext.Number.constrain(helperEl.dom.clientWidth + offset, me.minWidth, me.maxWidth));
	            }
	        }, this);
	    }
	});
    
}