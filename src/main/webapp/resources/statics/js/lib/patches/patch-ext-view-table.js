/**
 * ExtJS 4.2.1 버그픽스
 * 
 * @author jhkang
 * @since 2014.04.11
 */
if (Ext.getVersion().match('4.2.1')) {
	
	/**
	 * Grid Cell의 emptyCellText가 올바르게 적용되지 않는 문제
	 */
	Ext.override(Ext.view.Table, {
		
		/**
         * @private
         * Emits the HTML representing a single grid cell into the passed output stream (which is an array of strings).
         *
         * @param {Ext.grid.column.Column} column The column definition for which to render a cell.
         * @param {Number} recordIndex The row index (zero based within the {@link #store}) for which to render the cell.
         * @param {Number} columnIndex The column index (zero based) for which to render the cell.
         * @param {String[]} out The output stream into which the HTML strings are appended.
         */
        renderCell: function(column, record, recordIndex, columnIndex, out) {
            var me = this,
                selModel = me.selModel,
                cellValues = me.cellValues,
                classes = cellValues.classes,
                fieldValue = record.data[column.dataIndex],
                cellTpl = me.cellTpl,
                value, clsInsertPoint;
    
            cellValues.record = record;
            cellValues.column = column;
            cellValues.recordIndex = recordIndex;
            cellValues.columnIndex = columnIndex;
            cellValues.cellIndex = columnIndex;
            cellValues.align = column.align;
            cellValues.tdCls = column.tdCls;
            cellValues.innerCls = column.innerCls;
            cellValues.style = cellValues.tdAttr = "";
            cellValues.unselectableAttr = me.enableTextSelection ? '' : 'unselectable="on"';
    
            if (column.renderer && column.renderer.call) {
                value = column.renderer.call(column.scope || me.ownerCt, fieldValue, cellValues, record, recordIndex, columnIndex, me.dataSource, me);
                if (cellValues.css) {
                    // This warning attribute is used by the compat layer
                    // TODO: remove when compat layer becomes deprecated
                    record.cssWarning = true;
                    cellValues.tdCls += ' ' + cellValues.css;
                    delete cellValues.css;
                }
            } else {
                value = fieldValue;
            }
            cellValues.value = (value == null || value === '') ? column.emptyCellText : value;
    
            // Calculate classes to add to cell
            classes[1] = Ext.baseCSSPrefix + 'grid-cell-' + column.getItemId();
                
            // On IE8, array[len] = 'foo' is twice as fast as array.push('foo')
            // So keep an insertion point and use assignment to help IE!
            clsInsertPoint = 2;
    
            if (column.tdCls) {
                classes[clsInsertPoint++] = column.tdCls;
            }
            if (me.markDirty && record.isModified(column.dataIndex)) {
                classes[clsInsertPoint++] = me.dirtyCls;
            }
            if (column.isFirstVisible) {
                classes[clsInsertPoint++] = me.firstCls;
            }
            if (column.isLastVisible) {
                classes[clsInsertPoint++] = me.lastCls;
            }
            if (!me.enableTextSelection) {
                classes[clsInsertPoint++] = Ext.baseCSSPrefix + 'unselectable';
            }
    
            classes[clsInsertPoint++] = cellValues.tdCls;
            if (selModel && selModel.isCellSelected && selModel.isCellSelected(me, recordIndex, columnIndex)) {
                classes[clsInsertPoint++] = (me.selectedCellCls);
            }
    
            // Chop back array to only what we've set
            classes.length = clsInsertPoint;
    
            cellValues.tdCls = classes.join(' ');
    
            cellTpl.applyOut(cellValues, out);
            
            // Dereference objects since cellValues is a persistent var in the XTemplate's scope chain
            cellValues.column = null;
        }
		
	});
    
}