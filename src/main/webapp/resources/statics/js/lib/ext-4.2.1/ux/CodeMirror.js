/**
 * Fixed CodeMirror Editor
 * 
 * @author jhkang
 * @since 2012.09.21
 */
Ext.define('Ext.ux.CodeMirror', {
	
	extend: 'Ext.form.field.TextArea',
	
	alias: 'widget.metacodemirror',
	
	/**
	 * @cfg {String} mode The default mode to use when the editor is initialized. When not given, this will default to the first mode that was loaded.
	 * It may be a string, which either simply names the mode or is a MIME type associated with the mode. Alternatively,
	 * it may be an object containing configuration options for the mode, with a name property that names the mode
	 * (for example {name: "javascript", json: true}). The demo pages for each mode contain information about what
	 * configuration parameters the mode supports.
	 */
	mode: 'text/plain',
	
	/**
	 * @cfg {Boolean} Determines whether brackets are matched whenever the cursor is moved next to a bracket.
	 */
	enableMatchBrackets: false,
	
	/**
	 * @cfg {Boolean} enableElectricChars Configures whether the editor should re-indent the current line when a character is typed
	 * that might change its proper indentation (only works if the mode supports indentation). 
	 */
	enableElectricChars: true,
	
	/**
	 * @cfg {Boolean} enableSmartIndent Whether to use the context-sensitive indentation that the mode provides (or just indent the same as the line before).
	 */
	enableSmartIndent: true,
	
	/**
	 * @cfg {Boolean} enableIndentWithTabs Whether, when indenting, the first N*tabSize spaces should be replaced by N tabs.
	 */
	enableIndentWithTabs: false, 
	
	/**
	 * @cfg {Boolean} enableLineNumbers Whether to show line numbers to the left of the editor.
	 */
	enableLineNumbers: false,
	
	/**
	 * @cfg {Boolean} enableLineWrapping Whether CodeMirror should scroll or wrap for long lines.
	 */
	enableLineWrapping: false,
	
	/**
	 * @cfg {Boolean} enableGutter Can be used to force a 'gutter' (empty space on the left of the editor) to be shown even
	 * when no line numbers are active. This is useful for setting markers.
	 */
	enableGutter: true,
	
	/**
	 * @cfg {Boolean} enableFixedGutter When enabled (off by default), this will make the gutter stay visible when the
	 * document is scrolled horizontally.
	 */
	enableFixedGutter: false,
	
	/**
	 * @cfg {Number} firstLineNumber At which number to start counting lines.
	 */
	firstLineNumber: 1,
	
	/**
	 * @cfg {Boolean} readOnly <tt>true</tt> to mark the field as readOnly.
	 */
	readOnly : false,
	
	/**
	 * @cfg {Number} pollInterval Indicates how quickly (miliseconds) CodeMirror should poll its input textarea for changes.
	 * Most input is captured by events, but some things, like IME input on some browsers, doesn't generate events
	 * that allow CodeMirror to properly detect it. Thus, it polls.
	 */
	pollInterval: 100,
	
	/**
	 * @cfg {Number} indentUnit How many spaces a block (whatever that means in the edited language) should be indented.
	 */
	indentUnit: 4,
	
	/**
	 * @cfg {Number} tabSize The width of a tab character.
	 */
	tabSize: 4,
	
	/**
	 * @cfg {String} theme The theme to style the editor with. You must make sure the CSS file defining the corresponding
	 * .cm-s-[name] styles is loaded (see the theme directory in the distribution). The default is "default", for which
	 * colors are included in codemirror.css. It is possible to use multiple theming classes at once—for example
	 * "foo bar" will assign both the cm-s-foo and the cm-s-bar classes to the editor.
	 */
	theme: 'default',
	
	/**
	 * @property {String} pathModes Path to the modes folder to dinamically load the required scripts. You could also
	 * include all your required modes in a big script file and this path will be ignored.
	 * Do not fill in the trailing slash.
	 */
	pathModes: 'mode',
	
	/**
	 * @property {String} pathExtensions Path to the extensions folder to dinamically load the required scripts. You could also
	 * include all your required extensions in a big script file and this path will be ignored.
	 * Do not fill in the trailing slash.
	 */
    pathExtensions: 'lib/util',
	
    /**
     * @cfg {Boolean} enableLineHighlighting 
     */
    enableLineHighlighting: true,
    
    /**
     * @property {Number} hlLine
     */
    hlLine: 0,
    
    /**
     * @cfg {String} lastPos 
     */
    lastPos: null,
    
    /**
     * @cfg {String} lastQuery 
     */
    lastQuery: null,
    
    /**
     * @cfg {Array} marked 
     */
    marked: [],
    
    /**
     * @property {Array} modes Define here mode script dependencies; When choosing a specific mode the script files are automatically loaded
     */
	modes: [{
		mime: ['text/plain'],
		dependencies: []
	},{
		mime: ['application/x-httpd-php', 'text/x-php'],
		dependencies: ['xml/xml.js', 'javascript/javascript.js', 'css/css.js', 'clike/clike.js', 'php/php.js']
	},{
		mime: ['text/javascript', 'application/json'],
		dependencies: ['javascript/javascript.js']
	},{
		mime: ['text/html', 'application/xml'],
		dependencies: ['xml/xml.js', 'javascript/javascript.js', 'css/css.js', 'htmlmixed/htmlmixed.js']
	},{
		mime: ['text/css'],
		dependencies: ['css/css.js']
	},{
        mime: ['text/x-sql'],
        dependencies: ['sql/sql.js']
    },{
        mime: ['text/x-csrc', 'text/x-csharp', 'text/x-java'],
        dependencies: ['clike/clike.js']
    },{
        mime: ['application/x-ejs', 'application/x-aspx', 'application/x-jsp'],
        dependencies: ['xml/xml.js', 'javascript/javascript.js', 'css/css.js', 'htmlmixed/htmlmixed.js', 'htmlembedded/htmlembedded.js']
    }],
	
    scriptsLoaded: [],
    
	initComponent: function() {
		var me = this;
		
		me.addEvents(
			/**
             * @event initialize
             * Fires when the editor is fully initialized (including the iframe)
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'initialize',
			/**
             * @event activate
             * Fires when the editor is first receives the focus. Any insertion must wait
             * until after this event.
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'activate',
            /**
             * @event deactivate
             * Fires when the editor looses the focus. 
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'deactivate',
            /**
             * @event cursoractivity
             * Fires when the cursor or selection moves, or any change is made to the editor content.
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'cursoractivity',
            /**
             * @event gutterclick
             * Fires whenever the editor gutter (the line-number area) is clicked. 
             * @param {Ext.ux.form.field.CodeMirror} this
             * @param {Number} lineNumber Zero-based number of the line that was clicked
             * @param {Object} event The raw mousedown event
             */
            'gutterclick',
            /**
             * @event scroll
             * Fires whenever the editor is scrolled.
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'scroll',
            /**
             * @event highlightcomplete
             * Fires whenever the editor's content has been fully highlighted.
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'highlightcomplete',
            /**
             * @event update
             * Fires whenever CodeMirror updates its DOM display.
             * @param {Ext.ux.form.field.CodeMirror} this
             */
            'update',
            /**
             * @event keyevent
             * Fires on every keydown, keyup, and keypress event that CodeMirror captures.
             * @param {Ext.ux.form.field.CodeMirror} this
             * @param {Object} event This key event is pretty much the raw key event, except that a stop() method is always 
             * added to it. You could feed it to, for example, jQuery.Event to further normalize it. This function can inspect 
             * the key event, and handle it if it wants to. It may return true to tell CodeMirror to ignore the event. 
             * Be wary that, on some browsers, stopping a keydown does not stop the keypress from firing, whereas on others 
             * it does. If you respond to an event, you should probably inspect its type property and only do something when 
             * it is keydown (or keypress for actions that need character data).
             */
            'keyevent'
		);
		
		this.callParent(arguments);
	},
	
	/**
	 * 에디터 초기화
	 */
	initEditor: function() {
		var me = this,
            mode = 'text/plain';
        
        // if no mode is loaded we could get an error like "Object #<Object> has no method 'startState'"
        // search mime to find script dependencies
        var item = me.getMime(me.mode);
        if (item) {
            mode = me.getMimeMode(me.mode);
            if (!mode){
                mode = 'text/plain';
            }
        }
		
		this.editor = CodeMirror.fromTextArea(document.getElementById(this.getId()), {
        	matchBrackets: me.enableMatchBrackets,
        	electricChars: me.enableElectricChars,
        	autoClearEmptyLines: true,
        	indentUnit: me.indentUnit,
        	smartIndent: me.enableSmartIndent,
        	indentWithTabs: me.enableIndentWithTabs,
        	lineNumbers: me.enableLineNumbers,
        	lineWrapping: me.enableLineWrapping,
        	firstLineNumber: me.firstLineNumber,
            tabSize: me.tabSize,
            gutter: me.enableGutter,
            fixedGutter: me.enableFixedGutter,
        	theme: me.theme,
        	mode: mode,
        	value: me.rawValue,
        	onCursorActivity: function(editor) {
            	if (me.enableLineHighlighting) {
                    editor.setLineClass(me.hlLine, null);
                    me.hlLine = editor.getCursor().line;
                    editor.setLineClass(editor.getCursor().line, null, 'activeline');
                }
            	
                me.fireEvent('cursoractivity', me);
            },
            onGutterClick: function(editor, line, event) {
                me.fireEvent('gutterclick', me, line, event);
            },
            onFocus: function(editor) {
            	if (me.enableLineHighlighting) {
                    editor.setLineClass(me.hlLine, null, 'activeline');
                }
            	
                me.fireEvent('activate', me);
            },
            onBlur: function(editor) {
                me.fireEvent('deactivate', me);
            },
            onScroll: function(editor) {
                me.fireEvent('scroll', me);
            },
            onHighlightComplete: function(editor) {
                me.fireEvent('highlightcomplete', me);
            },
            onUpdate: function(editor) {
                me.fireEvent('update', me);
            },
            onKeyEvent: function(editor, event) {
                event.cancelBubble = true;
                me.fireEvent('keyevent', me, event);
            }
        });
        
        this.setMode(this.mode);
        this.setReadOnly(this.readOnly);
        
        this.fireEvent('initialize', me);
	},
	
	/**
	 * CodeMirror TextArea 크기조정
	 */
	refreshSize: function() {
		var panelEl = this.up('panel').body;
		this.editor.setSize(panelEl.getWidth(), panelEl.getHeight());
		this.scrollIntoView();
	},
	
	/**
	 * @override
	 */
	onRender: function() {
        this.callParent(arguments);
        this.initEditor();
        
        this.rendered = true;
	},
	
	/**
	 * @override
	 */
	onBoxReady: function() {
		this.refreshSize();
		this.callParent(arguments);
	},
	
	/**
	 * Set the editor as read only
	 * 
	 * @param {Boolean} readOnly
	 */
	setReadOnly: function(readOnly) {
        var me = this;
        
        if (me.editor){
            me.editor.setOption('readOnly', readOnly);
        }
    },
    
    /**
     * @override
     */
    onDisable: function() {
        this.bodyEl.mask();
        this.callParent(arguments);
    },

    /**
     * @override
     */
    onEnable: function() {
        this.bodyEl.unmask();
        this.callParent(arguments);
    },
	
    /**
     * Sets a data value into the field and runs the change detection. 
     * 
     * @param {Mixed} value The value to set
     * @return {Ext.ux.CodeMirror} this
     */
    setValue: function(value) {
        var me = this;
        
        me.mixins.field.setValue.call(me, value);
        me.rawValue = value;
        if (me.editor && (typeof value === 'string' || typeof value === 'number' )) {
            me.editor.setValue(value);
        }
        
        return me;
    },
    
    /**
     * 선택범위의 텍스트를 변환
     * @param {String} value 변환할 텍스트
     */
    replaceRange: function(value) {
    	var cursor = this.editor.getCursor(),
			selection = this.editor.getSelection(),
			from = {
    			line: cursor.line,
    			ch: cursor.ch - selection.length
    		},
    		to = {
    			line: cursor.line,
    			ch: cursor.ch
    		};
		
		this.editor.replaceRange(value, from, to);
    },
    
    /**
     * Return submit value to the owner form.
     * 
     * @return {Mixed} The field value
     */
    getSubmitValue: function() {
        return this.getValue();
    },
    
    /**
     * Return the value of the CodeMirror editor
     * 
     * @return {Mixed} The field value
     */
    getValue: function() {
        var me = this;
        
        if (me.editor) {
            return me.editor.getValue();
        } else {
            return null;
        }
    },
    
    /**
     * CodeMirror editor 에서 값을 return한다.
     * Query에 관련된 처리 작업에만 사용된다.
     * 
     * 1. delimiter가 없을 경우
     *  : 전체 값을 return한다.
     *  
     * 2. delimiter가 있을 경우
     *  2-1. selection이 존재하지 않을 경우
     *   : 현재 라인부터 앞뒤로 Loop를 돌면서 배열에 넣는다.
     *   : 앞으로 Loop를 돌 경우에는 Array의 앞에 집어넣는다. (unshift)
     *   : 뒤로 Loop를 돌 때는 Array의 뒤에 집어넣는다. (push)
     *   : 모두 조합한 후, 배열을 문자열로 바꾸어 return 한다.
     *  2-2. selection이 존재할 경우
     *   : selection된 값을 return 한다.
     * 
     * @param {String} delimiter
     * @return {Mixed} The field selection value
     * @author jhkang
     * @since 2012-02-28
     */
    getLineForQuery: function(delimiter) {
        var currentLine = this.editor.getCursor().line,
            maxLineCount = this.editor.lineCount(),
            query = [];
        
        // delimiter가 없을 경우, 모든 값을 return 한다.
        if (Ext.isEmpty(delimiter)) {
            return this.editor.getValue();
        } else {
            if (Ext.isEmpty(this.editor.getSelection())) {
                for (var i = currentLine ; i < maxLineCount ; i++) {
                    query.push(this.editor.getLine(i)); // Array의 뒤에 추가한다.
                    if (Meta.lib.string.endsWith(this.editor.getLine(i), delimiter)) {
                        break;
                    }
                }
                
                for (var i = (currentLine - 1) ; i >= 0 ; i--) {
                    if (Meta.lib.string.endsWith(this.editor.getLine(i), delimiter)) {
                        break;
                    } else {
                        query.unshift(this.editor.getLine(i)); // Array의 앞에 추가한다.
                    }
                }
                
                return query.join(' '); // ' '을 사용해 Array를 문자열로 바꿔서 조합한다.
            } else {
                return this.editor.getSelection();
            }
        }
    },
    
    /**
     * CodeMirror editor에서 검색된 값에 Mark한다.
     * 
     * @param {Ext.ux.form.field.CodeMirror} editor
     * @param {String} value 검색을 원하는 값
     * @author jhkang
     * @since 2012-02-27
     */
    doSearch: function(value) {
        this.unmark();
        
        var text = value;
        if (!text) {
            return;
        }
        
        for (var cursor = this.editor.getSearchCursor(text); cursor.findNext();) {
            this.marked.push(this.editor.markText(cursor.from(), cursor.to(), 'searched'));
        }
        
        if (this.lastQuery != text) {
            this.lastPos = null;
        }
        
        var cursor = this.editor.getSearchCursor(text, this.lastPos || this.editor.getCursor());
        
        if (!cursor.findNext()) {
            cursor = this.editor.getSearchCursor(text);
            
            if (!cursor.findNext()) {
                return;
            }
        }
        
        this.editor.setSelection(cursor.from(), cursor.to());
        this.lastQuery = text;
        this.lastPos = cursor.to();
    },
    
    /**
     * CodeMirror editor에서 Marking된 부분을 Unmarking 시킨다.
     * 
     * @author jhkang
     * @since 2012-02-27
     */
    unmark: function() {
        for (var i = 0, markedLength = this.marked.length ; i < markedLength ; ++i) {
            this.marked[i].clear();
        }
        this.marked.length = 0;
    },
    
    undo: function() {
        this.editor.undo();
    },
    
    redo: function() {
        this.editor.redo();
    },
    
    /**
     * 현재 커서의 위치를 리턴한다.
     * 
     * @return {Number}
     * @author jhkang
     * @since 2012-08-31
     */
    getCursor: function() {
    	return this.editor.getCursor().line + 1;
    },
    
    /**
     * 총 라인 수를 리턴한다.
     * 
     * @return {Number}
     * @author jhkang
     * @since 2012-08-31
     */
    getLineCount: function() {
    	return this.editor.lineCount();
    },
    
    /**
     * 라인 이동
     * 
     * @author jhkang
     * @since 2012-08-31
     */
//    moveLine: function(lineNum) {
//    	lineNum = parseInt( (lineNum<1)?1:lineNum );
//    	this.editor.setCursor(lineNum - 1, 0);
//    },
    
    moveLine: function(lineNum) {
    	if (typeof lineNum == 'object') {
	    	this.editor.setCursor({
	    		line: lineNum.line - this.firstLineNumber
	    	});
    	} else {
    		this.editor.setCursor({
	    		line: lineNum - this.firstLineNumber
	    	});
    	}
    },
    
	/**
	 * @private
	 */
	loadDependencies: function(item, path, handler, scope){
        var me = this;
        
        me.scripts = [];
        me.scriptIndex = -1;
        
        // load the dependencies
        for (var i = 0; i < item.dependencies.length ; i++) {
        	if (!Ext.Array.contains(me.scriptsLoaded, path + '/' + item.dependencies[i])) {
                var options = {
                    url: path + '/' + item.dependencies[i],
                    index: ++me.scriptIndex,
                    onLoad: function(options){
                        var ok = true;
                        for (j = 0 ; j < me.scripts.length ; j++){
                        	// this event could be raised before one script if fetched
                            if (me.scripts[j].called) {
                                ok = ok && me.scripts[j].success;
                                if (me.scripts[j].success && !Ext.Array.contains(me.scriptsLoaded, me.scripts[j].url)) {
                                	me.scriptsLoaded.push(me.scripts[j].url);
                                }
                            } else {
                                ok = false;
                            }
                        }
                        if (ok){
                            handler.call(scope || me.editor);
                        }
                    }
                };
            
                me.scripts[me.scriptIndex] = {
                    url: options.url,
                    success: true,
                    called: false,
                    options: options,
                    onLoad: options.onLoad || Ext.emptyFn,
                    onError: options.onError || Ext.emptyFn
                };
            }
        }
        for (var i = 0; i < me.scripts.length; i++){
            me.loadScript(me.scripts[i].options);
        }
    },
    
    /**
     * @private
     */
    loadScript: function(options){
        var me = this;
        
        Ext.Ajax.request({
        	url: options.url,
            scriptIndex: options.index,
            success: function(response, options) {
                var script = 'Ext.getCmp("' + this.id + '").scripts[' + options.scriptIndex + ']';
                window.setTimeout('try { ' + response.responseText + ' } catch(e) { '+script+'.success = false; '+script+'.onError('+script+'.options, e); };  ' + script + '.called = true; if ('+script+'.success) '+script+'.onLoad('+script+'.options);', 0);
            },
            failure: function(response, options) {
                var script = this.scripts[options.scriptIndex];
                script.success = false;
                script.called = true;
                script.onError(script.options, response.status);
            },
            scope: me
        });        
    },
    
    /**
     * @private
     */
    getMime: function(mime) {
        var me = this,
        	item,
        	found = false;
        
        for (var i = 0 ; i < me.modes.length ; i++){
            item = me.modes[i];
            if (Ext.isArray(item.mime)) {
                if (Ext.Array.contains(item.mime, mime)) {
                    found = true;
                    break;
                }
            } else {
                if (item === mime){
                    found = true;
                    break;
                }
            }
        }
        if (found) {
            return item;
        } else {
            return null;
        }
    },
    
    /**
     * @private
     * Return mode depending on the mime; If the mime is not loaded then return null
     * 
     * @param mime
     */
    getMimeMode: function(mime) {
        var mode = null;
        	mimes = CodeMirror.listMIMEs();
        for (var i = 0 ; i < mimes.length ; i++){
            if (mimes[i].mime === mime){
                mode = mimes[i].mode;
                if (typeof mode === 'object') {
                    mode = mode.name;
                }
                break;
            }
        }
        return mode;
    },
    
    /**
     * Change the CodeMirror mode to the specified mime.
     * 
     * @param {String} mime The MIME value according to the CodeMirror documentation
     */
    setMode: function(mime) {
        var me = this, 
            found = false,
            // search mime to find script dependencies
            item = me.getMime(mime);
        
        if (!item) {
            // mime not found
            return;
        }
        var mode = me.getMimeMode(mime);

        if (!mode){
            me.loadDependencies(item, me.pathModes, function(){
                var mode = me.getMimeMode(mime);
                if (typeof mode === 'string') {
                    me.editor.setOption('mode', mime);
                } else {
                    me.editor.setOption('mode', mode);
                }
            });
        } else {
            if (typeof mode === 'string') {
                me.editor.setOption('mode', mime);
            } else {
                me.editor.setOption('mode', mode);
            }
        }
        
        this.mode = mime;
    },
    
    /**
     * @return {String}
     */
    getMode: function() {
    	return this.mode;
    },
    
    scrollIntoView: function(pos) {
    	var editor = this.editor;
    	editor.scrollIntoView(pos || editor.getCursor());
    }
	
});