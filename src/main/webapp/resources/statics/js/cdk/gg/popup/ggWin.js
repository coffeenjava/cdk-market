Ext.define('cdk.gg.popup.ggWin', {
    
    alias: 'widget.gongguwindow',

    extend: 'cdk.common.popup.simple',
    
    title: '공구',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/gonggu'
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            modal: true,
            autoScroll: true,
            closeAction: 'destroy',
            width: 300,
            layout: 'fit',
            
            items: [{
                xtype: 'form',
                
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                
                items: [{
                    xtype: 'datefield',
                    name: 'ggSt',
                    fieldLabel: '시작일',
                    format: 'Y-m-d',
                    submitFormat: 'YmdHis',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false,
                    editable: false
                },{
                    xtype: 'datefield',
                    name: 'ggEnd',
                    fieldLabel: '종료',
                    format: 'Y-m-d',
                    submitFormat: 'YmdHis',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false,
                    editable: false
                }]
            }],
            
            buttons: [{
                text: '저장',
                scope: me,
                handler: me.fnSave
            },{
                text: '닫기',
                handler: function() {
                    me.close();
                }
            }],
            
            listeners: {
                scope: me,
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    boxReady: function() {
        var me = this,
            st, end;
        
        me.form = me.down('form').getForm();
        
        // update
        if (me.selectedRecord) {
            st = cm.stringToDate(me.selectedRecord.get('ggSt'));
            end = cm.stringToDate(me.selectedRecord.get('ggEnd'));
            
            me.form.setValues({
                ggSt: new Date(st),
                ggEnd: new Date(end)
            });
        }
    }
});