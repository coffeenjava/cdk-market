Ext.define('cdk.gg.ggList', {
    
    extend: 'Ext.panel.Panel',
    
    requires: [
        'cdk.gg.popup.ggWin'
    ],
    
    title: '공구 목록',
    
    constructor: function(config) {
        this.loadParams = {
            isAvail: true
        };
        this.requestUrl = 'admin/gonggu';
        this.popup = 'gongguwindow';

        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;
        
        me.ggStore = Ext.create('Ext.data.Store', {
            fields: [
                'ggKey',
                'ggSt',
                'ggEnd',
                'ggTotPrice',
                'creator'
            ],
            proxy: {
                type: 'ajax',
                url: me.requestUrl,
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        me.ggDetailStore = Ext.create('Ext.data.Store', {
            fields: [
                'prdCatNm',
                'prdNm',
                'orderCnt',
                'orderPrice',
                'orderOrgPrice'
            ]
        });
        
        Ext.apply(me, {
            layout: 'border',
            items: [{
                xtype: 'grid',
                itemId: 'ggGrid',
                region: 'center',
                flex: 1,
                
                dockedItems: [{
                    dock: 'top',
                    xtype: 'panel',
                    bodyPadding: 5,
                    layout: {
                        type: 'hbox',
                        pack: 'end'
                    },
                    items: [{
                        xtype: 'button',
                        text: '추가',
                        width: 100,
                        scope: me,
                        handler: me.fnShowAdd
                    },{
                        xtype: 'button',
                        text: '수정',
                        width: 100,
                        margin: '0 0 0 5',
                        scope: me,
                        handler: me.fnShowMod
                    }]
                }],
                
                store: me.ggStore,
                
                columns: {
                    defaults: {
                        style: 'text-align:center',
                        align: 'center',
                        flex: 1
                    },
                    
                    items: [{
                        text: '시작일',
                        dataIndex: 'ggSt',
                        renderer: function(v) {
                            return cm.getSimpleDateString(v);
                        }
                    },{
                        text: '종료일',
                        dataIndex: 'ggEnd',
                        renderer: function(v) {
                            return cm.getSimpleDateString(v);
                        }
                    },{
                        text: '판매액',
                        dataIndex: 'ggTotPrice',
                        renderer: Ext.util.Format.numberRenderer('0,000')
                    },{
                        text: '생성자',
                        dataIndex: 'creator'
                    }]
                },
                
                listeners: {
                    scope: me,
                    select: me.fnLoadDetail
                }
            },{
                xtype: 'grid',
                region: 'south',
                flex: 1,
                header: false,
                collapsible: true,
                split: true,
                
                store: me.ggDetailStore,
                
                columns: {
                    defaults: {
                        style: 'text-align:center',
                        align: 'center',
                        flex: 1
                    },
                    
                    items: [{
                        text: '제품 카테고리',
                        dataIndex: 'prdCatNm'
                    },{
                        text: '제품',
                        dataIndex: 'prdNm'
                    },{
                        text: '판매수',
                        dataIndex: 'orderCnt'
                    },{
                        text: '판매액',
                        dataIndex: 'orderPrice',
                        renderer: Ext.util.Format.numberRenderer('0,000')
                    },{
                        text: '판매액(원가)',
                        dataIndex: 'orderOrgPrice',
                        renderer: Ext.util.Format.numberRenderer('0,000')
                    }]
                }
            }],
            
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    },
    
    fnLoadDetail: function(row, model, idx) {
        var me = this;
        
        // url parameter 로 Ajax 호출하기
        // Ext.
        
        me.ggDetailStore.load({
            params: {
                ggKey: model.get('ggKey')
            }
        });
    },
    
    fnLoad: function() {
        this.ggStore.load({
            params: this.loadParams
        });
    },
    
    fnShowAdd: function() {
        this.showWin();
    },
    
    fnShowMod: function() {
        var selected = this.down('#ggGrid').getSelectionModel().getSelection();
        
        if (!Ext.isEmpty(selected)) {
            this.showWin(selected[0]);
        }
    },
    
    showWin: function(selectedRecord) {
        var me = this;
        
        Ext.widget(me.popup,{
            selectedRecord: selectedRecord,
            callback: function() {
                me.fnLoad();
            }
        }).show();
    }
});