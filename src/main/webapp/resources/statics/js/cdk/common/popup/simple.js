/**
 * Popup 공통 컴포넌트
 *
 *
 *** 컨트롤 목록
 * selectedRecord       update 일 경우 전달한다.
 *
 * paramInterceptor     상속받은 popup 에서 파라미터 조작이 필요한 경우 해당 함수를 정의한다.
 *                      fnSave 내부에서 호출한다.
 */
Ext.define('cdk.common.popup.simple', {
    
    extend: 'Ext.window.Window',
    
    modal: true,
    bodyPadding: 10,

    constructor: function(config) {
        this.requestUrl = config.requestUrl;
        this.callParent(arguments);
    },
    
    fnSave: function() {
        var me = this,
            params;
        
        if(!me.form.isValid()) {
            Ext.Msg.show({
                msg: '필수 항목을 모두 입력하십시오.',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
            return;
        }
        
        params = me.form.getValues();
        
        if (me.selectedRecord) {
            params = Ext.apply(me.selectedRecord.data, params);
        }

        // parameter 조작 처리
        if (me.paramInterceptor && typeof me.paramInterceptor === 'function') {
            me.paramInterceptor(params);
        }
        
        Ext.Ajax.request({
            url: me.requestUrl,
            method: me.selectedRecord ? 'PATCH' : 'POST',
//            headers: { 'Content-Type': 'application/json' },
            jsonData: params,
            success: function(form, action) {
                if(me.callback) {
                    me.callback();
                };
                me.close();
            }
        });
    }
});