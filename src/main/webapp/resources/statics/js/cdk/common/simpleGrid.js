Ext.define('cdk.common.simpleGrid', {
    
    extend: 'Ext.grid.Panel',
    
    constructor: function(config) {
        this.loadParams = config.loadParams || {};
        this.requestUrl = config.requestUrl;
        this.popup = config.popup;
        
        this.callParent(arguments);
    },
    
    fnLoad: function() {
        this.store.load({
            params: this.loadParams
        });
    },
    
    fnRemove: function() {
        var me = this,
            selected = me.getSelectionModel().getSelection();
            
        if (!Ext.isEmpty(selected)) {
            Ext.Msg.show({
                msg: '삭제하시겠습니까?',
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.YESNO,
                fn: function(btn) {
                    if (btn === 'yes') {
                        Ext.Ajax.request({
                            url: me.requestUrl,
                            method: 'DELETE',
                            jsonData: selected[0].data,
                            success: function(form, action) {
                                me.fnLoad();
                            }
                        });
                    }
                }
            });
        }
    },
    
    fnShowAdd: function() {
        this.showWin();
    },
    
    fnShowMod: function() {
        var selected = this.getSelectionModel().getSelection();
        
        if (!Ext.isEmpty(selected)) {
            this.showWin(selected[0]);
        }
    },
    
    showWin: function(selectedRecord) {
        var me = this;
        
        Ext.widget(me.popup,{
            selectedRecord: selectedRecord,
            callback: function() {
                me.fnLoad();
            }
        }).show();
    }
});