Ext.define('cdk.app', {
    extend: 'Ext.container.Viewport',
    
    requires: [
        'cdk.order.orderForm'
    ],
    
    layout: 'vbox',
    minWidth: 800,
    border: false,
    autoScroll: true,
    
    initComponent: function() {
        var me = this,
            phoneTypeStore,
            prdStore;
        
        phoneTypeStore = Ext.create('Ext.data.Store', {
            fields: ['CD'],
            data: [
                { CD: '010' },
                { CD: '011' },
                { CD: '016' },
                { CD: '017' },
                { CD: '019' }
            ]
        });
        
        prdStore = Ext.create('Ext.data.Store', {
            fields: ['prdCd','prdNm','prdPrice'],
            data: [
                { prdCd: '0001', prdNm: '스트로베리', prdPrice: 1000 },
                { prdCd: '0002', prdNm: '마스크', prdPrice: 2000 },
                { prdCd: '0003', prdNm: '키위', prdPrice: 3000 }
            ]
        });;
        
        me.orderStore = Ext.create('Ext.data.Store', {
            fields: ['prdCd','prdNm','prdPrice','prdCnt']
        });
        
        Ext.apply(me, {
            items: [{
                xtype: 'orderform'
            }]
        });
        
        me.callParent(arguments);
    }
});