Ext.define('cdk.app2', {
    extend: 'Ext.container.Viewport',
    
    layout: 'vbox',
    minWidth: 800,
    border: false,
    autoScroll: true,
    
    initComponent: function() {
        var me = this,
            phoneTypeStore,
            prdStore;
        
        phoneTypeStore = Ext.create('Ext.data.Store', {
            fields: ['CD'],
            data: [
                { CD: '010' },
                { CD: '011' },
                { CD: '016' },
                { CD: '017' },
                { CD: '019' }
            ]
        });
        
        prdStore = Ext.create('Ext.data.Store', {
            fields: ['prdCd','prdNm','prdPrice'],
            data: [
                { prdCd: '0001', prdNm: '스트로베리', prdPrice: 1000 },
                { prdCd: '0002', prdNm: '마스크', prdPrice: 2000 },
                { prdCd: '0003', prdNm: '키위', prdPrice: 3000 }
            ]
        });;
        
        me.orderStore = Ext.create('Ext.data.Store', {
            fields: ['prdCd','prdNm','prdPrice','prdCnt']
        });
        
        Ext.apply(me, {
            items: [{
                title: '주문서',
                xtype: 'form',
                border: false,
                width: 600,
                padding: '10',
                bodyStyle: 'padding:10px;',
                layout: {
                    type: 'anchor'
                },
                items: [{
                    title: '주문자 정보',
                    xtype: 'fieldset',
                    padding: '10',
                    layout: 'vbox',
                    items: [{
                        xtype: 'container',
                        layout : 'hbox',
                        items: [{
                            xtype: 'textfield',
                            name: 'payNm',
                            fieldLabel: '입금명',
                            labelWidth: 60,
                            width: 170,
                            allowBlank: false
                        },{
                            xtype: 'fieldcontainer',
                            fieldLabel: '전화번호',
                            labelWidth: 60,
                            margin: '0 0 0 50',
                            layout: 'hbox',
                            items: [{
                                xtype: 'combo',
                                name : 'phone1',
                                displayField: 'CD',
                                valueField: 'CD',
                                editable: false,
                                width: 60,
                                allowBlank: false,
                                store: phoneTypeStore,
                                listeners: {
                                    boxready: function() {
                                        this.setValue(this.getStore().getAt(0)); // 로딩 후 첫번째 아이템 선택
                                    }
                                }
                            },{
                                xtype: 'displayfield',
                                value: '-',
                                margin: '0 0 0 5'
                            },{
                                xtype: 'textfield',
                                name: 'phone2',
                                allowBlank: false,
                                validator: function(v) {
                                    return /^\d{0,4}$/.test(v);
                                },
                                width: 45,
                                margin: '0 0 0 5',
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: me.removeLastChar
                                }
                            },{
                                xtype: 'displayfield',
                                value: '-',
                                margin: '0 0 0 5'
                            },{
                                xtype: 'textfield',
                                name: 'phone3',
                                allowBlank: false,
                                validator: function(v) {
                                    return /^\d{0,4}$/.test(v);
                                },
                                width: 45,
                                margin: '0 0 0 5',
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: me.removeLastChar
                                }
                            }]
                        }]
                    },{
                        xtype: 'textfield',
                        name: 'address',
                        flex: 1,
                        fieldLabel: '주소',
                        labelWidth: 60,
                        width: 470,
                        allowBlank: false,
                        margin: '10 0 0 0'
                    }]
                },{
                    title: '제품 정보',
                    xtype: 'fieldset',
                    padding: '5',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        felx: 1,
                        items: [{
                            xtype: 'combo',
                            name : 'prdNm',
                            fieldLabel: '제품명',
                            displayField: 'prdNm',
                            valueField: 'prdCd',
                            editable: false,
                            labelWidth: 60,
                            width: 200,
                            store: prdStore,
                            listeners: {
                                boxready: function() {
                                    this.setValue(this.getStore().getAt(0));
                                },
                                change: function(combo, nVal, oVal) {
                                    var record = this.getStore().findRecord('prdCd',nVal);
                                    this.up('form').getForm().findField('prdPrice').setValue(record.get('prdPrice'));
                                }
                            }
                        },{
                            xtype: 'displayfield',
                            name: 'prdPrice',
                            fieldLabel: '가격',
                            labelWidth: 30,
                            renderer: Ext.util.Format.numberRenderer('0,000'),
                            value: 0,
                            margin: '0 0 0 30'
                        },{
                            xtype: 'combo',
                            name : 'prdCnt',
                            fieldLabel: '개수',
                            displayField: 'prdCnt',
                            valueField: 'prdCnt',
                            editable: false,
                            labelWidth: 60,
                            width: 120,
                            margin: '0 0 0 50',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['prdCnt'],
                                data: (function() {
                                    var data = [];
                                    for (var i=1;i<=100; i++) {
                                        data.push({ prdCnt: i });
                                    }
                                    return data;
                                })()
                            }),
                            listeners: {
                                boxready: function() {
                                    this.setValue(this.getStore().getAt(0));
                                }
                            }
                        },{
                            xtype: 'button',
                            text: '선택',
                            minWidth: 50,
                            margin: '0 0 0 10',
                            scope: me,
                            handler: me.onSelectPrd
                        }]
                    },{
                        xtype: 'grid',
                        itemId: 'gridPrd',
                        title: '선택된 제품 내역',
                        margin: '10 0 0 0',
//                        viewConfig : {
//                            emptyText: cm.emptyText('선택된 제품이 없습니다.')
//                        },
                        store: me.orderStore,
                        columns: {
                            defaults: {
                                style: 'text-align:center',
                                align: 'center'
                            },
                            
                            items: [{
                                text: '제품명',
                                dataIndex: 'prdNm',
                                flex: 1
                            },{
                                text: '개수',
                                dataIndex: 'prdCnt'
                            },{
                                text: '가격',
                                dataIndex: 'prdPrice',
                                renderer: Ext.util.Format.numberRenderer('0,000')
                            },{
                                xtype: 'actioncolumn',
                                text: '취소',
                                tdCls: 'component-column',
                                items: [{
                                    iconCls: 'icon-delete',
                                    handler: me.onDeletePrd
                                }]
                            }]
                        }
                    }]
                },{
                    xtype: 'container',
                    anchor: '100%',
                    layout: {
                        type: 'hbox'
                    },
                    items: [{
                        xtype: 'tbspacer',
                        flex: 1
                    },{
                        xtype: 'button',
                        text: '저장',
                        minWidth: 80,
                        scope: me,
                        handler: me.onSave
                    }]
                }]
            }],
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
        
        me.form = me.down('form').getForm();
        
        me.form.findField('payNm').focus();
    },
    
    /**
     * 제품 선택
     */
    onSelectPrd: function() {
        var me = this,
            comboPrdNm = me.form.findField('prdNm'),
            prdCd, prdNm, prdPrice, prdCnt;
        
        prdCd = comboPrdNm.getValue();
        prdNm = comboPrdNm.getRawValue();
        prdCnt = me.form.findField('prdCnt').getValue();
        prdPrice = me.form.findField('prdPrice').getValue() * prdCnt;
        
        if (me.orderStore.findRecord('prdCd',prdCd)) {
            cm.notice('[선택된 제품 내역]에 동일한 제품이 있습니다.\n기존 내역을 [취소]하고 선택해주세요.');
            return;
        }
        
        me.orderStore.add({
            prdCd: prdCd,
            prdNm: prdNm,
            prdPrice: prdPrice,
            prdCnt: prdCnt
        });
    },
    
    /**
     * 선택 목록의 제품 삭제
     */
    onDeletePrd: function(grid, rowIdx, colIdx, e, el, model) {
        grid.getStore().remove(model);
    },
    
    /**
     * 주문서 저장
     */
    onSave: function() {
        var me = this,
            userInfo,
            orderInfoArr = [];
        
        if (!me.form.isValid()) {
            cm.notice('필수 입력 항목을 모두 입력해주세요.');
            return;
        }
        
        if (me.orderStore.count() === 0) {
            cm.notice('주문하신 제품이 없습니다.');
            return;
        }
        
        userInfo = me.getUserInfo();
        
        Ext.Array.each(me.orderStore.getRange(),function(item) {
            orderInfoArr.push(item.data);
        });
        
        Ext.Ajax.request({
            url: 'order',
            method: 'POST',
            params: userInfo,
            jsonData: orderInfoArr,
            success: function(res) {
                debugger;
            }
        });
    },
    
    getUserInfo: function() {
        var me = this,
            form = me.form,
            info;
        
        return {
            payNm: form.findField('payNm').getValue(),
            phone: form.findField('phone1').getValue() + '-' + form.findField('phone2').getValue() + '-' + form.findField('phone3').getValue(),
            address: form.findField('address').getValue()
        }
    },
    
    removeLastChar: function() {
        var txt = this.getValue();
        if (!this.isValid()) {
            this.setValue(txt.substring(0,txt.length-1));
        }
    }
});