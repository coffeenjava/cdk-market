Ext.define('cdk.customer.customerList', {
    
    extend: 'cdk.common.simpleGrid',
    
    requires: [
        'cdk.customer.popup.customerWin'
    ],
    
    title: '고객목록',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/customer',
            popup: 'customerwindow',
            loadParams: {
                isAvail: true
            }
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;
        
        me.customerStore = Ext.create('Ext.data.Store', {
            fields: [
                'custKey',
                'payNm',
                'custNm',
                'phone',
                'address',
                'creator',
                'availSt',
                'availEnd'
            ],
            proxy: {
                type: 'ajax',
                url: me.requestUrl,
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        Ext.apply(me, {
            dockedItems: [{
                dock: 'top',
                xtype: 'panel',
                bodyPadding: 5,
                margin: '0 10 0 0',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [{
                    xtype: 'button',
                    text: '추가',
                    width: 100,
                    scope: me,
                    handler: me.fnShowAdd
                },{
                    xtype: 'button',
                    text: '수정',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnShowMod
                }]
            }],
            
            store: me.customerStore,
            
            columns: {
                defaults: {
                    style: 'text-align:center',
                    align: 'center',
                    flex: 1
                },
                
                items: [{
                    text: '전화번호',
                    dataIndex: 'phone'
                },{
                    text: '입금명',
                    dataIndex: 'payNm'
                },{
                    text: '고객명',
                    dataIndex: 'custNm'
                },{
                    text: '주소',
                    dataIndex: 'address'
                },{
                    text: '생성자',
                    dataIndex: 'creator'
                },{
                    text: '생성일',
                    flex: 1.5,
                    dataIndex: 'availSt',
                    renderer: function(v) {
                        return cm.getDateString(v);
                    }
                },{
                    text: '종료일',
                    flex: 1.5,
                    dataIndex: 'availEnd',
                    renderer: function(v) {
                        return cm.getDateString(v);
                    }
                }]
            },
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    }
});