Ext.define('cdk.customer.popup.customerWin', {
    
    alias: 'widget.customerwindow',
    
    extend: 'cdk.common.popup.simple',
    
    title: '고객',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/customer'
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;
            
        Ext.apply(me, {
            modal: true,
            autoScroll: true,
            closeAction: 'destroy',
            width: 300,
            layout: 'fit',
            
            items: [{
                xtype: 'form',
                
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                
                items: [{
                    xtype: 'textfield',
                    name: 'phone',
                    fieldLabel: '전화번호',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false
                },{
                    xtype: 'textfield',
                    name: 'payNm',
                    fieldLabel: '입금명',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false
                },{
                    xtype: 'textfield',
                    name: 'custNm',
                    fieldLabel: '고객명',
                    labelWidth: 60,
                    width: 170
                },{
                    xtype: 'textfield',
                    name: 'address',
                    fieldLabel: '주소',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false
                }]
            }],
            
            buttons: [{
                text: '저장',
                scope: me,
                handler: me.fnSave
            },{
                text: '닫기',
                handler: function() {
                    me.close();
                }
            }],
            
            listeners: {
                scope: me,
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    boxReady: function() {
        var me = this;
        
        me.form = me.down('form').getForm();
        
        // update
        if (me.selectedRecord) {
            me.form.loadRecord(me.selectedRecord);
            me.form.findField('phone').setDisabled(true);
        }
    }
});