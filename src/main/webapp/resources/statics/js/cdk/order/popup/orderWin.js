Ext.define('cdk.order.popup.orderWin', {
    
    alias: 'widget.orderwindow',

    extend: 'Ext.window.Window',

    title: '주문 내역',
    modal: true,
    bodyPadding: 10,
    
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            modal: true,
            autoScroll: true,
            closeAction: 'destroy',
            width: 300,
            layout: 'fit',
            
            items: [{
                xtype: 'form',
                
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            }],
            
            buttons: [{
                text: '주문',
                scope: me,
                handler: me.fnSave
            },{
                text: '취소',
                handler: function() {
                    me.close();
                }
            }],
            
            listeners: {
                scope: me,
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    boxReady: function() {
        var me = this,
            form = me.down('form');

        Ext.Array.forEach(me.orderInfoArr, function(item) {
            form.add({
                xtype: 'fieldcontainer',
                fieldLabel: item.prdNm,
                layout: 'hbox',
                items: [{
                    html: item.prdSellCnt + ' 개'
                }]
            });
        });

        form.add({
            xtype: 'fieldcontainer',
            fieldLabel: '총 주문금액',
            layout: 'hbox',
            items: [{
                html: me.totalPrice + '원'
            }]
        });
    }
});