Ext.define('cdk.order.orderForm', {
    extend: 'Ext.form.Panel',
    
    alias: 'widget.orderform',

    requires: [
        'cdk.order.popup.orderWin'
    ],
    
    title: '주문서',
    border: false,
    width: 600,
    padding: '10',
    bodyStyle: 'padding:10px;',
    layout: {
        type: 'anchor'
    },
    
    initComponent: function() {
        var me = this,
            phoneTypeStore;
        
        phoneTypeStore = Ext.create('Ext.data.Store', {
            fields: ['CD'],
            data: [
                { CD: '010' },
                { CD: '011' },
                { CD: '016' },
                { CD: '017' },
                { CD: '018' },
                { CD: '019' }
            ]
        });
        
        me.prdStore = Ext.create('Ext.data.Store', {
            fields: [
                'prdKey',
                'prdCatCd',
                'prdCatNm',
                'prdNm',
                'prdOrgPrice',
                'prdPrice',
                'prdPer',
                'prdCnt',
                'prdSellCnt',
                'prdCancelCnt',
                'prdOldCnt',
                'prdImg',
                'creator',
                'availSt',
                'availEnd'
            ],
            proxy: {
                type: 'ajax',
                url: 'admin/product',
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        me.orderStore = Ext.create('Ext.data.Store', {
            fields: ['prdKey','prdCd','prdNm','prdPrice','prdSellCnt']
        });
        
        Ext.apply(me, {
            items: [{
                title: '주문자 정보',
                xtype: 'fieldset',
                padding: '10',
                layout: 'vbox',
                items: [{
                    xtype: 'displayfield',
                    value: '이메일과 전화번호 입력 후 [과거정보 불러오기] 를 반드시 클릭해주세요.',
                    fieldCls: 'warning-text'
                },{
                    xtype: 'fieldcontainer',
                    margin: '10 0 0 0',
                    layout: 'hbox',
                    items: [{
                        xtype: 'textfield',
                        name: 'email',
                        fieldLabel: '이메일',
                        labelWidth: 60,
                        width: 300,
                        allowBlank: false
                    },{
                        xtype: 'button',
                        itemId: 'btnChgInfo',
                        text: '이메일/전화번호 변경하기',
                        width: 160,
                        margin: '0 0 0 10',
                        disabled: true,
                        scope: me,
                        handler: me.changeInfo
                    }]
                },{
                    xtype: 'fieldcontainer',
                    fieldLabel: '전화번호',
                    labelWidth: 60,
                    margin: '10 0 0 0',
                    layout: 'hbox',
                    items: [{
                        xtype: 'combo',
                        name : 'phone1',
                        displayField: 'CD',
                        valueField: 'CD',
                        editable: false,
                        width: 60,
                        allowBlank: false,
                        store: phoneTypeStore,
                        listeners: {
                            boxready: function() {
                                this.setValue(this.getStore().getAt(0)); // 로딩 후 첫번째 아이템 선택
                            }
                        }
                    },{
                        xtype: 'displayfield',
                        value: '-',
                        margin: '0 0 0 5'
                    },{
                        xtype: 'textfield',
                        name: 'phone2',
                        allowBlank: false,
                        validator: function(v) {
                            return /^\d{0,4}$/.test(v);
                        },
                        width: 45,
                        margin: '0 0 0 5',
                        enableKeyEvents: true,
                        listeners: {
                            keyup: me.removeLastChar
                        }
                    },{
                        xtype: 'displayfield',
                        value: '-',
                        margin: '0 0 0 5'
                    },{
                        xtype: 'textfield',
                        name: 'phone3',
                        allowBlank: false,
                        validator: function(v) {
                            return /^\d{0,4}$/.test(v);
                        },
                        width: 45,
                        margin: '0 0 0 5',
                        enableKeyEvents: true,
                        listeners: {
                            scope: me,
                            keyup: me.removeLastChar
                        }
                    },{
                        xtype: 'button',
                        itemId: 'btnLoadInfo',
                        text: '과거정보 불러오기',
                        width: 100,
                        margin: '0 0 0 10',
                        scope: me,
                        handler: me.fnSearchCustomer
                    }]
                },{
                    xtype: 'textfield',
                    name: 'payNm',
                    fieldLabel: '입금명',
                    labelWidth: 60,
                    width: 170,
                    disabled: true,
                    allowBlank: false,
                    margin: '10 0 0 0'
                },{
                    xtype: 'textfield',
                    name: 'address',
                    flex: 1,
                    fieldLabel: '주소',
                    labelWidth: 60,
                    width: 470,
                    disabled: true,
                    allowBlank: false,
                    margin: '10 0 0 0'
                }]
            },{
                title: '제품 정보',
                xtype: 'fieldset',
                padding: '5',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    felx: 1,
                    items: [{
                        xtype: 'combo',
                        name : 'prdNm',
                        fieldLabel: '제품명',
                        displayField: 'prdNm',
                        valueField: 'prdKey',
                        editable: false,
                        labelWidth: 60,
                        width: 200,
                        disabled: true,
                        store: me.prdStore,
                        listeners: {
                            select: function(combo, records) {
                                me.form.findField('prdPrice').setValue(records[0].get('prdPrice'));
                            }
                        }
                    },{
                        xtype: 'displayfield',
                        name: 'prdPrice',
                        fieldLabel: '가격',
                        labelWidth: 30,
                        renderer: Ext.util.Format.numberRenderer('0,000'),
                        value: 0,
                        margin: '0 0 0 30'
                    },{
                        xtype: 'combo',
                        name : 'prdCnt',
                        fieldLabel: '개수',
                        displayField: 'prdCnt',
                        valueField: 'prdCnt',
                        editable: false,
                        labelWidth: 60,
                        width: 120,
                        disabled: true,
                        margin: '0 0 0 50',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['prdCnt'],
                            data: (function() {
                                var data = [];
                                for (var i=1;i<=100; i++) {
                                    data.push({ prdCnt: i });
                                }
                                return data;
                            })()
                        }),
                        listeners: {
                            boxready: function() {
                                this.setValue(this.getStore().getAt(0));
                            }
                        }
                    },{
                        xtype: 'button',
                        text: '추가',
                        minWidth: 50,
                        margin: '0 0 0 10',
                        scope: me,
                        handler: me.onSelectPrd
                    }]
                },{
                    xtype: 'grid',
                    itemId: 'gridPrd',
                    title: '선택된 제품 내역',
                    margin: '10 0 0 0',
//                        viewConfig : {
//                            emptyText: cm.emptyText('선택된 제품이 없습니다.')
//                        },
                    store: me.orderStore,
                    columns: {
                        defaults: {
                            style: 'text-align:center',
                            align: 'center'
                        },
                        
                        items: [{
                            text: '제품명',
                            dataIndex: 'prdNm',
                            flex: 1
                        },{
                            text: '개수',
                            dataIndex: 'prdSellCnt'
                        },{
                            text: '가격',
                            dataIndex: 'prdPrice',
                            renderer: Ext.util.Format.numberRenderer('0,000')
                        },{
                            xtype: 'actioncolumn',
                            text: '취소',
                            tdCls: 'component-column',
                            items: [{
                                iconCls: 'icon-delete',
                                handler: me.onDeletePrd
                            }]
                        }]
                    }
                }]
            },{
                xtype: 'container',
                anchor: '100%',
                layout: {
                    type: 'hbox'
                },
                items: [{
                    xtype: 'displayfield',
                    fieldLabel: '총 주문금액',
                    itemId: 'totalPrice',
                    value: '0'
                },{
                    xtype: 'tbspacer',
                    flex: 1
                },{
                    xtype: 'button',
                    text: '주문하기',
                    minWidth: 80,
                    scope: me,
                    handler: me.onSave
                }]
            }],
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
        
        me.form = me.getForm();
        
        cm.focus(me.form.findField('email'));
    },
    
    /**
     * 제품 선택
     */
    onSelectPrd: function() {
        var me = this,
            comboPrdNm = me.form.findField('prdNm'),
            prdKey, prdNm, prdPrice, prdCnt, totalPrice;
        
        prdKey = comboPrdNm.getValue();
        prdNm = comboPrdNm.getRawValue();
        prdCnt = me.form.findField('prdCnt').getValue();
        prdPrice = me.form.findField('prdPrice').getValue() * prdCnt;

        if (me.orderStore.findRecord('prdKey',prdKey)) {
            cm.notice('[선택된 제품 내역]에 동일한 제품이 있습니다.\n기존 내역을 [취소]하고 선택해주세요.');
            return;
        }

        totalPrice = me.down('#totalPrice');

        totalPrice.setValue(cm.toMoney(cm.moneyToNum(totalPrice.getValue()) + prdPrice));

        me.orderStore.add({
            prdKey: prdKey,
            prdNm: prdNm,
            prdPrice: prdPrice,
            prdSellCnt: prdCnt
        });
    },
    
    /**
     * 선택 목록의 제품 삭제
     */
    onDeletePrd: function(grid, rowIdx, colIdx, e, el, model) {
        grid.getStore().remove(model);
    },

    changeInfo: function() {
        this.initForm();
        this.down('#btnLoadInfo').setDisabled(false);
        this.down('#btnChgInfo').setDisabled(true);
        this.disableKeyFields(false);
        this.disableEtcFields(true);
    },

    initForm: function() {
        this.form.reset();
    },
    
    /**
     * 주문서 저장
     */
    onSave: function() {
        var me = this,
            userInfo,
            orderInfoArr = [];
        
        if (!me.form.isValid()) {
            cm.notice('필수 입력 항목을 모두 입력해주세요.');
            return;
        }
        
        if (me.orderStore.count() === 0) {
            cm.notice('주문하신 제품이 없습니다.');
            return;
        }
        
        userInfo = me.getCustomerInfo();
        
        Ext.Array.each(me.orderStore.getRange(),function(item) {
            orderInfoArr.push(item.data);
        });

        Ext.widget('orderwindow',{
            orderInfoArr: orderInfoArr,
            totalPrice: me.down('#totalPrice').getValue(),
            fnSave: function() {
                Ext.Msg.show({
                    msg: '주문하시겠습니까?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btnId) {
                        if (btnId === 'yes') {
                            Ext.Ajax.request({
                                url: 'order',
                                method: 'POST',
                                params: userInfo,
                                jsonData: orderInfoArr,
                                success: function(res) {
                                    cm.notice('감사합니다. 주문이 완료되었습니다.');
                                }
                            });
                        }
                    }
                });
            }
        }).show();
    },
    
    fnLoadCustomer: function(customer) {
        var me = this,
            form = me.form;
            
        form.setValues(customer);
    },
    
    fnSearchCustomer: function() {
        var me = this,
            btnLoadInfo = me.down('#btnLoadInfo'),
            btnChgInfo = me.down('#btnChgInfo'),
            customer;

        if (!me.isPhoneValid()) {
            return;
        }

        btnLoadInfo.setDisabled(true);
        btnChgInfo.setDisabled(false);
        me.disableKeyFields(true);
        me.disableEtcFields(false);
        
        customer = me.getCustomerInfo();
        
        Ext.Ajax.request({
            url: 'admin/customer/' + customer.email + '/' + customer.phone,
            method: 'GET',
            success: function(form, action) {
                if (!form.responseText) {
                    cm.notice('과거주문정보가 없습니다. 새롭게 입력해주세요.');
                    return;
                }
                
                me.fnLoadCustomer(Ext.decode(form.responseText));
            }
        });
    },

    disableKeyFields: function(disable) {
        var me = this,
            form = me.form;

        form.findField('email').setDisabled(disable);
        form.findField('phone1').setDisabled(disable);
        form.findField('phone2').setDisabled(disable);
        form.findField('phone3').setDisabled(disable);
    },

    disableEtcFields: function(disable) {
        var me = this,
            form = me.form;

        form.findField('payNm').setDisabled(disable);
        form.findField('address').setDisabled(disable);
        form.findField('prdNm').setDisabled(disable);
        form.findField('prdCnt').setDisabled(disable);
    },
    
    isPhoneValid: function() {
        var me = this;
            form = me.form;
            
        if (!form.findField('phone2').getValue() || !form.findField('phone3').getValue()) {
            cm.notice('전화번호를 입력 후 클릭해주세요.');
            return false;
        }
        
        return true;
    },
    
    getCustomerInfo: function() {
        var me = this,
            form = me.form;

        return {
            email: form.findField('email').getValue(),
            payNm: form.findField('payNm').getValue(),
            phone: form.findField('phone1').getValue() + '-' + form.findField('phone2').getValue() + '-' + form.findField('phone3').getValue(),
            address: form.findField('address').getValue()
        }
    },
    
    removeLastChar: function(field) {
        var txt = field.getValue();
        if (!field.isValid()) {
            field.setValue(txt.substring(0,txt.length-1));
        }
    }
});