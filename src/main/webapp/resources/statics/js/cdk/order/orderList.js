Ext.define('cdk.order.orderList', {
    extend: 'Ext.grid.Panel',
    
    title: '주문내역',
    
    initComponent: function() {
        var me = this,
            searchConditionStore;
        
        searchConditionStore = Ext.create('Ext.data.Store', {
            fields: [
                'ggKey',
                'ggSt',
                'ggEnd',
                'ggTotPrice',
                'creator',
                'availSt',
                'availEnd'
            ],
            proxy: {
                type: 'ajax',
                url: 'admin/gonggus',
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        me.orderInfoStore = Ext.create('Ext.data.Store', {
            fields: [
                'ggKey',
                'custKey',
                'phone',
                'payNm',
                'address',
                'prdKey',
                'prdCatNm',
                'payNm',
                'orderCnt',
                'orderPrice',
                'orderStatus',
                'statusChDt',
                'creator',
                'availSt',
                'availEnd'
            ],
            proxy: {
                type: 'ajax',
                url: 'admin/orders',
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: false
        });
        
//        Ext.create('Ext.data.Store', {
//            fields: ['ggKey'],
//            proxy: {
//                type: 'ajax',
//                url: 'admin/orders',
//                actionMethods: 'GET',
//                reader: {
//                    type: 'json',
//                    root: 'rows'
//                }
//            },
//            autoLoad: false
//        });
        
        Ext.apply(me, {
            dockedItems: [{
                xtype: 'form',
                dock: 'top',
                layout: {
                    type: 'hbox'
//                    ,
//                    pack: 'end'
                },
                padding: '5',
                style: {
                    backgroundColor: '#F7F7F7'
                },
                items: [{
                    xtype: 'combo',
                    name : 'ggKey',
                    fieldLabel: '회차',
                    displayField: 'ggKey',
                    valueField: 'ggKey',
                    editable: false,
                    labelWidth: 60,
                    width: 200,
                    store: searchConditionStore,
                    listeners: {
                        select: function(combo, records) {
                            me.orderInfoStore.load({
                                params: {
                                    ggKey: records[0].get('ggKey')
                                } 
                            });
                        }
                    }
                },{
                }]
            }],
            
            store: me.orderInfoStore,
            
            columns: {
                defaults: {
                    style: 'text-align:center',
                    align: 'center',
                    flex: 1
                },
                
                items: [{
                    text: '입금명',
                    dataIndex: 'payNm'
                },{
                    text: '고객명',
                    dataIndex: 'custNm'
                },{
                    text: '주문내역', // 제품명(개수)
                    dataIndex: 'orderList'
                },{
                    text: '주문금액',
                    dataIndex: 'orderPrice',
                    renderer: Ext.util.Format.numberRenderer('0,000')
                },{
                    text: '주문시간',
                    dataIndex: 'createDt'
                },{
                    text: '상태',
                    dataIndex: 'status'
                }]
            },
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    }
});