var cm = cm || {};

Ext.apply(cm, {
    emptyText: function(txt) {
        var eTxt = '<div style="display: table; width: 100%; height: 99%; #position: relative; overflow: hidden;"><div style="#position: absolute; #top: 50%; display: table-cell; vertical-align: middle; text-align: center;"><div style="#position: relative; #top: -50%;"><font color="gray">';
        eTxt += txt;
        eTxt += '</font></div></div></div>';
        return eTxt;
    },
    
    notice: function(msg) {
        Ext.Msg.show({
            title: '알림',
            msg: msg,
            closable: false,
            icon: Ext.Msg.INFO,
            buttons: Ext.Msg.OK
        });
    },
    
    createTab: function(id) {
        return Ext.create(id,{
            id: id,
            closable: true
        })
    },
    
    focus: function(el) {
        Ext.defer(function() {
            el.focus();
        }, 100);
    },
    
    getDateString: function(str) {
        var dt = str.substr(0,4) + '-' + str.substr(4,2) + '-' + str.substr(6,2);
        return dt += ' ' + str.substr(8,2) + ':' + str.substr(10,2) + ':' + str.substr(12,2);
    },
    
    getSimpleDateString: function(str) {
        return str.substr(0,4) + '-' + str.substr(4,2) + '-' + str.substr(6,2);
    },
    
    stringToDate: function(str) {
        var dt = str.substr(0,4) + '-' + str.substr(4,2) + '-' + str.substr(6,2);
        return dt += 'T' + str.substr(8,2) + ':' + str.substr(10,2) + ':' + str.substr(12,2);
    },

    toMoney: function(num) {
        return Ext.util.Format.number(num, '0,000');
    },

    moneyToNum: function(money) {
        return Number(money.replace(/,/gi,''));
    }
});