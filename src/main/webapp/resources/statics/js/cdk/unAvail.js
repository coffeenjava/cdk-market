Ext.define('cdk.unAvail', {
    extend: 'Ext.container.Viewport',
    
    layout: 'fit',
    minWidth: 800,
    border: false,
    autoScroll: true,
    
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            items: [{
                html: '<div style="font-size:30px;text-align:center;margin-top:30px;">공구 기간이 아닙니다.</div>'
            }]
        });
        
        me.callParent(arguments);
    }
});