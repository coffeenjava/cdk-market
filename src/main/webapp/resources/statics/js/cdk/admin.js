Ext.define('cdk.admin', {
    extend: 'Ext.container.Viewport',
    
    layout: 'border',
    minWidth: 800,
    border: false,
    autoScroll: true,
    
    initComponent: function() {
        var me = this,
            menuStore;
        
        menuStore = Ext.create('Ext.data.Store', {
            fields: ['NM','ID'],
            data: [
                { NM: '주문내역', ID: 'cdk.order.orderList' },
                { NM: '고객관리', ID: 'cdk.customer.customerList' },
                { NM: '제품관리', ID: 'cdk.prd.prdList' },
                { NM: '제품카테고리관리', ID: 'cdk.prd.catList' },
                { NM: '공구관리', ID: 'cdk.gg.ggList' },
                { NM: '작업로그' },
                { NM: '주문하기', ID: 'cdk.order.orderForm' }
            ]
        });
        
        Ext.apply(me, {
            items: [{
                xtype: 'grid',
                region: 'west',
                flex: .2,
                header: false,
                collapsible: true,
                split: true,
                store: menuStore,
                columns: {
                    items: [{
                        text: '메뉴',
                        dataIndex: 'NM',
                        flex: 1
                    }]
                },
                listeners: {
                    scope: me,
                    select: me.menuSelect
                }
            },{
                xtype: 'tabpanel',
                region: 'center',
                flex: 1,
                minTabWidth: 130,
                plugins: [
                    Ext.create('Ext.ux.TabCloseMenu', {
                        closeTabText: '닫기',
                        closeOthersTabsText: '다른 탭 닫기',
                        closeAllTabsText: '모든 탭 닫기'
                    }),
                    Ext.create('Ext.ux.TabReorderer')
                ],
                listeners: {
                    close: function(tab) {
                        Ext.ComponentMgr.unregister(tab.getId());
                    },
                    tabchange: function(comp, tab) {
                        var tabs = [],
                            ownerCt = comp.ownerCt,
                            oldToken, newToken,
                            activeIndex = comp.items.findIndex('id', comp.getActiveTab().getId());
                        
                        tabs.push(tab.title);
                        tabs.push(tab.id);
                        
                        newToken = tabs.reverse().join(':');
                        oldToken = Ext.History.getToken();
                        
                        if (oldToken === null || oldToken.search(newToken) === -1) {
                            Ext.History.add(newToken, true);
                        }
                    }
                }
            }],
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function(comp) {
        var me = this;
        
        me.tab = me.down('tabpanel');
        me.tab.add(cm.createTab('cdk.prd.prdList'));
    },
    
    menuSelect: function(grid, selected, idx, eOpts) {
        var id = selected.get('ID'),
            comp = this.tab.getComponent(id);
        
        if (!comp) {
            comp = cm.createTab(id);
            this.tab.add(comp);
        }
        
        this.tab.setActiveTab(comp);
        
    }
});