Ext.define('cdk.prd.catList', {
    
    extend: 'cdk.common.simpleGrid',
    
    requires: [
        'cdk.prd.popup.catWin'
    ],
    
    title: '카테고리 목록',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/product/category',
            popup: 'categorywindow',
            loadParams: {
                isAvail: true
            }
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;
        
        me.store = Ext.create('Ext.data.Store', {
            fields: [
                'prdCatCd',
                'prdCatNm',
                'creator',
                'availSt'
            ],
            proxy: {
                type: 'ajax',
                url: me.requestUrl,
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        Ext.apply(me, {
            dockedItems: [{
                dock: 'top',
                xtype: 'panel',
                bodyPadding: 5,
                margin: '0 10 0 0',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [{
                    xtype: 'button',
                    text: '추가',
                    width: 100,
                    scope: me,
                    handler: me.fnShowAdd
                },{
                    xtype: 'button',
                    text: '수정',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnShowMod
                },{
                    xtype: 'button',
                    text: '삭제',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnRemove
                }]
            }],
            
            store: me.store,
            
            columns: {
                defaults: {
                    style: 'text-align:center',
                    align: 'center',
                    flex: 1
                },
                
                items: [{
                    text: '카테고리 코드',
                    dataIndex: 'prdCatCd'
                },{
                    text: '카테고리명',
                    dataIndex: 'prdCatNm'
                },{
                    text: '생성자',
                    dataIndex: 'creator'
                },{
                    text: '생성일',
                    dataIndex: 'availSt',
                    renderer: function(v) {
                        return cm.getDateString(v);
                    }
                }]
            },
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    }
});