Ext.define('cdk.prd.popup.catWin', {
    
    alias: 'widget.categorywindow',
    
    extend: 'cdk.common.popup.simple',
    
    title: '제품 카테고리',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/product/category'
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            modal: true,
            autoScroll: true,
            closeAction: 'destroy',
            width: 300,
            layout: 'fit',
            
            items: [{
                xtype: 'form',
                
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                
                items: [{
                    xtype: 'textfield',
                    name: 'prdCatNm',
                    fieldLabel: '카테고리명',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false,
                    enableKeyEvents: true,
                    listeners: {
                        scope: me,
                        specialKey: function(t, e) {
                            if (e.getKey() === e.ENTER) {
                                me.fnSave();
                            }
                        }
                    }
                }]
            }],
            
            buttons: [{
                text: '저장',
                scope: me,
                handler: me.fnSave
            },{
                text: '닫기',
                handler: function() {
                    me.close();
                }
            }],
            
            listeners: {
                scope: me,
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    boxReady: function() {
        var me = this;
        
        me.form = me.down('form').getForm();
        
        // update
        if (me.selectedRecord) {
            me.form.loadRecord(me.selectedRecord);
        }
        
        cm.focus(me.form.findField('prdCatNm'));
    }
});