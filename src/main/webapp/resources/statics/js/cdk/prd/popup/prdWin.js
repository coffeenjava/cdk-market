Ext.define('cdk.prd.popup.prdWin', {
    
    alias: 'widget.productwindow',
    
    extend: 'cdk.common.popup.simple',
    
    title: '제품',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/product'
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;
            
        me.catStore = Ext.create('Ext.data.Store', {
            fields: [
                'prdCatCd',
                'prdCatNm',
                'creator',
                'availSt'
            ],
            proxy: {
                type: 'ajax',
                url: me.requestUrl+'/category',
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        Ext.apply(me, {
            modal: true,
            autoScroll: true,
            closeAction: 'destroy',
            width: 300,
            layout: 'fit',
            
            items: [{
                xtype: 'form',
                
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                
                items: [{
                    xtype: 'combo',
                    name : 'prdCatCd',
                    fieldLabel: '카테고리',
                    labelWidth: 60,
                    width: 170,
                    displayField: 'prdCatNm',
                    valueField: 'prdCatCd',
                    editable: false,
                    allowBlank: false,
                    store: me.catStore
                },{
                    xtype: 'textfield',
                    name: 'prdNm',
                    fieldLabel: '제품명',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false
                },{
                    xtype: 'numberfield',
                    name: 'prdCnt',
                    fieldLabel: '초기개수',
                    maxValue: 1000,
                    minValue: 0,
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false
                },{
                    xtype: 'numberfield',
                    name: 'prdOrgPrice',
                    fieldLabel: '원가',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false,
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false
                },{
                    xtype: 'numberfield',
                    name: 'prdPer',
                    fieldLabel: '마진율',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false,
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false
                },{
                    xtype: 'numberfield',
                    name: 'prdPrice',
                    fieldLabel: '판매가',
                    labelWidth: 60,
                    width: 170,
                    allowBlank: false,
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false
                }]
            }],
            
            buttons: [{
                text: '저장',
                scope: me,
                handler: me.fnSave
            },{
                text: '닫기',
                handler: function() {
                    me.close();
                }
            }],
            
            listeners: {
                scope: me,
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    boxReady: function() {
        var me = this;
        
        me.form = me.down('form').getForm();
        
        // update
        if (me.selectedRecord) {
            me.form.loadRecord(me.selectedRecord);
            me.form.findField('prdCatCd').setDisabled(true);
            me.form.findField('prdNm').setDisabled(true);
        }
    }
});