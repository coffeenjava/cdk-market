Ext.define('cdk.prd.prdList', {
    
    extend: 'cdk.common.simpleGrid',
    
    requires: [
        'cdk.prd.popup.prdWin'
    ],
    
    title: '제품목록',
    
    constructor: function(config) {
        Ext.apply(config, {
            requestUrl: 'admin/product',
            popup: 'productwindow',
            loadParams: {
                isAvail: true
            }
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;
        
        me.prdStore = Ext.create('Ext.data.Store', {
            fields: [
                'prdKey',
                'prdCatCd',
                'prdCatNm',
                'prdNm',
                'prdOrgPrice',
                'prdPrice',
                'prdPer',
                'prdCnt',
                'prdSellCnt',
                'prdCancelCnt',
                'prdImg',
                'creator',
                'availSt',
                'availEnd'
            ],
            proxy: {
                type: 'ajax',
                url: me.requestUrl,
                methods: 'GET',
                reader: {
                    type: 'json',
                    root: 'rows'
                }
            },
            autoLoad: true
        });
        
        Ext.apply(me, {
            dockedItems: [{
                dock: 'top',
                xtype: 'panel',
                bodyPadding: 5,
                margin: '0 10 0 0',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [{
                    xtype: 'button',
                    text: '추가',
                    width: 100,
                    scope: me,
                    handler: me.fnShowAdd
                },{
                    xtype: 'button',
                    text: '수정',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnShowMod
                },{
                    xtype: 'button',
                    text: '삭제',
                    width: 100,
                    margin: '0 0 0 5',
                    scope: me,
                    handler: me.fnRemove
                }]
            }],
            
            store: me.prdStore,
            
            columns: {
                defaults: {
                    style: 'text-align:center',
                    align: 'center',
                    flex: 1
                },
                
                items: [{
                    text: '이미지',
                    dataIndex: 'prdImg'
                },{
                    text: '카테고리',
                    dataIndex: 'prdCatNm'
                },{
                    text: '제품명',
                    dataIndex: 'prdNm'
                },{
                    text: '판매가',
                    dataIndex: 'prdPrice',
                    renderer: Ext.util.Format.numberRenderer('0,000')
                },{
                    text: '원가',
                    dataIndex: 'prdOrgPrice',
                    renderer: Ext.util.Format.numberRenderer('0,000')
                },{
                    text: '마진율',
                    dataIndex: 'prdPer'
                },{
                    text: '초기개수',
                    dataIndex: 'prdCnt'
                },{
                    text: '판매개수',
                    dataIndex: 'prdSellCnt'
                },{
                    text: '취소개수',
                    dataIndex: 'prdCancelCnt'
                },{
                    text: '생성자',
                    dataIndex: 'creator'
                },{
                    text: '생성일',
                    flex: 1.5,
                    dataIndex: 'availSt',
                    renderer: function(v) {
                        return cm.getDateString(v);
                    }
                }]
            },
            listeners: {
                boxready: me.boxReady
            }
        });
        
        me.callParent(arguments);
    },
    
    /**
     * Initialize
     */
    boxReady: function() {
        var me = this;
    }
});