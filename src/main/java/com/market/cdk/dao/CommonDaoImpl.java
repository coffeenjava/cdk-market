package com.market.cdk.dao;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;

@Repository
public class CommonDaoImpl<T,E> implements CommonDao<T> {
    
    @Autowired
    private SqlSession sqlSession;

    @Override
    public int selectInt(String mapperId, T params) throws DataAccessException {
        return sqlSession.selectOne(mapperId, params);
    }
    
    @Override
    public Object selectOne(String mapperId, T params) throws DataAccessException {
        return sqlSession.selectOne(mapperId, params);
    }

    @Override
    public Optional<?> selectOptionalOne(String mapperId, T params) throws DataAccessException {
        return Optional.ofNullable(sqlSession.selectOne(mapperId, params));
    }
    
    @Override
    public Map<String,Object> selectRow(String mapperId, T params) throws DataAccessException {
        return sqlSession.selectOne(mapperId, params);
    }
    
    @Override
    public List<E> selectList(String mapperId, T params) throws DataAccessException {
        return sqlSession.selectList(mapperId, params);
    }
    
    @Override
    public int insert(String mapperId, T params) throws DataAccessException {
        return sqlSession.insert(mapperId, params);
    }
    
    @Override
    public int update(String mapperId, T params) throws DataAccessException {
        return sqlSession.update(mapperId, params);
    }
}