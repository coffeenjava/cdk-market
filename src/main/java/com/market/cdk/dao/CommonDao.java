package com.market.cdk.dao;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;

import org.springframework.dao.DataAccessException;

public interface CommonDao<T> {
    int selectInt(String mapperId, T params) throws DataAccessException;
    Object selectOne(String mapperId, T params) throws DataAccessException;
    Optional<?> selectOptionalOne(String mapperId, T params) throws DataAccessException;
    Map<String,Object> selectRow(String mapperId, T params) throws DataAccessException;
    List selectList(String mapperId, T params) throws DataAccessException;

    int insert(String mapperId, T params) throws DataAccessException;

    int update(String mapperId, T params) throws DataAccessException;
}