package com.market.cdk.controller;

import java.text.DateFormat;
import java.util.*;

import com.market.cdk.service.GongguService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@Controller
public class AppController {
    private static final Logger logger = LoggerFactory.getLogger(AppController.class);

    @Autowired
    GongguService gongguService;
    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String app(Locale locale, Model model) {
        logger.info("Welcome home! The client locale is {}.", locale);
        
//        Date date = new Date();
//        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
//
//        String formattedDate = dateFormat.format(date);

        model.addAttribute("view", "cdk.unAvail");

        // 진행 중인 공구가 있는지 확인
        gongguService.getAvailGonggu()
                .ifPresent(i -> model.addAttribute("view", "cdk.app"));

        return "index";
    }
    
    /**
     * 
     * @param payNm
     * @param phone
     * @param address
     * @param orderList [{prdCd,prdCnt}]
     * @return
     */
//    @RequestMapping(value = "/order", method = RequestMethod.POST)
//    @ResponseBody
//    public Object order(@RequestParam("payNm") String payNm,
//                        @RequestParam("phone") String phone,
//                        @RequestParam("address") String address,
//                        @RequestBody List<Map<String,String>> orderList) {
//        
//        System.out.println(payNm);
//        // 0. Insert User
//        // 기존 주문자 아닐 경우 <-- 신규 주문자 여부도 파라미터로 받을지 고민
//        
//        // 1. Select Product Info
//        // 제품 정보 + 카테고리 불러오기
//        
//        // 2. 재고 개수 확인
//        // 재고 개수 부족할 경우 Error message return
//        
//        // 3. 주문 데이터 생성
//        
//        
//        // 2. Insert Order
//        // 제품
//        Map<String,Object> m = new HashMap<>();
//        Map<String,Object> m2 = new HashMap<>();
//        m.put("name", payNm);
//        m.put("phone", phone);
//        m2.put("a", phone);
//        m2.put("b", phone);
//        m.put("inner", m2);
//        return m;
//    }
}
