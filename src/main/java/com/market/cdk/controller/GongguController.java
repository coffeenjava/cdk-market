package com.market.cdk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.market.cdk.bean.Gonggu;
import com.market.cdk.dao.CommonDao;
import com.market.cdk.service.GongguService;

import lombok.extern.log4j.Log4j;

/**
 * Handles requests for the application home page.
 */
@Controller
@Log4j
@RequestMapping("/admin/gonggu")
public class GongguController {
    
    @Autowired
    CommonDao commonDao;
    
    @Autowired
    GongguService gongguService;
    
    @RequestMapping(value = "/{ggKey}", method = RequestMethod.GET)
    @ResponseBody
    public Object selectDetail(@PathVariable int ggKey) {
        return commonDao.selectOne("gonggu.selectDetail", ggKey);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Gonggu> selectList(@RequestParam(defaultValue = "true") boolean isAvail) {
        return gongguService.selectList(isAvail);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void insert(@RequestBody Gonggu gonggu) {
        // TODO user 가져오기
        gonggu.setCreator("system");
        gongguService.insert(gonggu);
    }

    @RequestMapping(method = RequestMethod.PATCH)
    @ResponseBody
    public void update(@RequestBody Gonggu gonggu) {
        gongguService.update(gonggu);
    }
}
