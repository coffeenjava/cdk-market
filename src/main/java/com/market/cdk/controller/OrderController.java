package com.market.cdk.controller;

import java.util.List;
import java.util.Optional;

import com.market.cdk.service.GongguService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.market.cdk.bean.Customer;
import com.market.cdk.bean.Order;
import com.market.cdk.bean.Product;
import com.market.cdk.service.OrderService;

import lombok.extern.log4j.Log4j;

@Controller
@Log4j
public class OrderController {
    
    @Autowired
    OrderService orderService;

    @Autowired
    GongguService gongguService;

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    @ResponseBody
    public void order(@RequestParam("email") String email,
                      @RequestParam("phone") String phone,
                      @RequestParam("payNm") String payNm,
                      @RequestParam("address") String address,
                      @RequestBody List<Product> productList) {

        /**
         * 가능한 공구가 있는지 확인
         *
         * TODO Response 처리
         */
        Optional<Integer> ggKey = gongguService.getAvailGonggu();

        if (ggKey.isPresent() == false) {
            return;
        }

        Customer customer = Customer.builder()
                                .email(email)
                                .phone(phone)
                                .payNm(payNm)
                                .address(address)
                                .build();

        orderService.order(customer, productList, ggKey.get());
    }
    
    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    @ResponseBody
    public List<Order> selectList(@RequestParam("ggKey") int ggKey,
                                    @RequestParam(value="isAvail", defaultValue = "true") boolean isAvail) {
        return orderService.selectList(ggKey, isAvail);
    }
}
