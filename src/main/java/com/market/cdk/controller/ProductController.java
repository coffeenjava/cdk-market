package com.market.cdk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.market.cdk.bean.Category;
import com.market.cdk.bean.Product;
import com.market.cdk.service.ProductService;

import lombok.extern.log4j.Log4j;

/**
 * Handles requests for the application home page.
 */
@Controller
@Log4j
@RequestMapping("/admin/product")
public class ProductController {
    
    @Autowired
    ProductService productService;
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Product> selectList(@RequestParam(defaultValue = "true") boolean isAvail) {
        return productService.selectList(isAvail);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void insert(@RequestBody Product product) {
        // TODO user 가져오기
        product.setCreator("system");
        productService.insert(product);
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@RequestBody Product product) {
        productService.delete(product);
    }
    
    @RequestMapping(method = RequestMethod.PATCH)
    @ResponseBody
    public void update(@RequestBody Product product) {
        productService.update(product);
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> selectCategoryList(@RequestParam(defaultValue = "true") boolean isAvail) {
        return productService.selectCategoryList(isAvail);
    }
    
    @RequestMapping(value = "/category", method = RequestMethod.POST)
    @ResponseBody
    public void insertCategory(@RequestBody Category category) {
        category.setCreator("brian");
        productService.insertCategory(category);
    }
    
    @RequestMapping(value = "/category", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteCategory(@RequestBody Category category) {
        productService.deleteCategory(category);
    }
    
    @RequestMapping(value = "/category", method = RequestMethod.PATCH)
    @ResponseBody
    public void updateCategory(@RequestBody Category category) throws Exception {
        productService.updateCategory(category);
    }
}
