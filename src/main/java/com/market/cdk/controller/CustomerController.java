package com.market.cdk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.market.cdk.bean.Customer;
import com.market.cdk.dao.CommonDao;
import com.market.cdk.service.CustomerService;

import lombok.extern.log4j.Log4j;

/**
 * Handles requests for the application home page.
 */
@Controller
@Log4j
@RequestMapping("/admin/customer")
public class CustomerController {
    
    @Autowired
    CommonDao commonDao;
    
    @Autowired
    CustomerService customerService;
    
    @RequestMapping(value = "/{email}/{phone}", method = RequestMethod.GET)
    @ResponseBody
    public Customer select(@PathVariable String email, @PathVariable String phone) {
        Customer customer = Customer.builder()
                .email(email)
                .phone(phone)
                .build();
        return customerService.select(customer);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Customer> selectList(@RequestParam(defaultValue = "true") boolean isAvail) {
        return customerService.selectList(isAvail);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void insert(@RequestBody Customer customer) {
        // TODO user 가져오기
        customer.setCreator("system");
        customerService.insert(customer);
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@RequestBody Customer customer) {
        customerService.delete(customer);
    }
    
    @RequestMapping(method = RequestMethod.PATCH)
    @ResponseBody
    public void update(@RequestBody Customer customer) {
        customerService.update(customer);
    }
}
