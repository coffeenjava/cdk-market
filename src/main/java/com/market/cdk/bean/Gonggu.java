package com.market.cdk.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gonggu {
    private int ggKey;
    private String ggSt;
    private String ggEnd;
    private String creator;
}
