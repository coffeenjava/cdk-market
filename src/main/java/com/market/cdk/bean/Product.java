package com.market.cdk.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private String prdKey;
    private String prdCatCd;
    private String prdCatNm;
    private String prdNm;
    private int prdOrgPrice;
    private int prdPrice;
    private int prdPer;
    private int prdCnt;
    private int prdSellCnt;
    private int prdCancelCnt;
    private String prdImg;
    private String creator;
    private String availSt;
    private String availEnd;
    
    public void makePrdKey() {
        this.setPrdKey(prdCatCd + "-" + prdNm);
    }
    
    public boolean isPriceChanged(Product product) {
        if (this.prdOrgPrice == product.getPrdOrgPrice()
                && this.prdPrice == product.getPrdPrice()) {
            return false;
        }
        return true;
    }
    
    public boolean isPrdCntChanged(Product product) {
        if (this.prdCnt == product.getPrdCnt()) {
            return false;
        }
        return true;
    }
}
