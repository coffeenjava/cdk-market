package com.market.cdk.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    private String prdCatCd;
    private String prdCatNm;
    private String creator;
    private String availSt;
    private String availEnd;
    
    public void makeCatCd(Optional<String> catCd) {
        int iCatCd = Integer.parseInt(catCd.orElse("0000"));
        this.setPrdCatCd(String.format("%04d", ++iCatCd));
    }
}
