package com.market.cdk.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GongguDetail {
    private int ggDetailKey;
    private int ggKey;
    private String prdCatNm;
    private int prdKey;
    private String prdNm;
    private int orderCnt;
    private int orderPrice;
    private int orderOrgPrice;
}
