package com.market.cdk.bean;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    private int orderKey;
    private int ggKey;
    private int custKey;
    private String creator;
    private String availSt;
    private String availEnd;

    // product_order_stat
    private String orderStatus;

    // product_order_detail
    private String prdKey;
    private int orderCnt;

    @Getter
    public enum Status {
        START("ST", "시작"),
        ONGOING("OG", "접수완료"),
        PACKAGED("PK", "포장완료"),
        COMPLETE("CP", "배송완료");

        private String code;
        private String value;

        Status(String code, String value) {
            this.code = code;
            this.value = value;
        }
    }
}
