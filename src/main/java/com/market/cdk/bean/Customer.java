package com.market.cdk.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    private int custKey;
    private String email;
    private String payNm;
    private String custNm;
    private String phone;
    private String address;
    private String creator;
    private String availSt;
    private String availEnd;
    
    public boolean equal(Customer c) {
        if (this.phone.equals(c.getPhone())
                && this.payNm.equals(c.getPayNm())
                && this.address.equals(c.getAddress())) {
            if (c.getCustNm() != null && !c.getCustNm().equals(this.custNm)) {
                return false;
            }
            return true;
        }
        return false;
    }
}
