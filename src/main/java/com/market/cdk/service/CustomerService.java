package com.market.cdk.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.market.cdk.bean.Customer;

public interface CustomerService {
    Customer getCustomerForOrder(Customer customer) throws DataAccessException;
    
    Customer select(Customer customer) throws DataAccessException;
    
    List<Customer> selectList(boolean isAvail) throws DataAccessException;
    
    void delete(Customer customer) throws DataAccessException;
    
    void insert(Customer customer) throws DataAccessException;
    
    void update(Customer customer) throws DataAccessException;
}
