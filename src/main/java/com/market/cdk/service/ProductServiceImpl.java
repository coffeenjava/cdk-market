package com.market.cdk.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.market.cdk.bean.Category;
import com.market.cdk.bean.Product;
import com.market.cdk.dao.CommonDao;

@Service
@SuppressWarnings("unchecked")
public class ProductServiceImpl implements ProductService {
    @Autowired
    CommonDao commonDao;

    @Override
    public Product select(String prdKey) throws DataAccessException {
        return (Product) commonDao.selectOne("product.select", prdKey);
    };
    
    @Override
    public List<Product> selectList(boolean isAvail) throws DataAccessException {
        return commonDao.selectList("product.selectList", isAvail);
    };
    
    @Override
    public void delete(Product product) throws DataAccessException {
        commonDao.update("product.delete", product);
    };
    
    @Override
    public void insert(Product product) throws DataAccessException {
        product.makePrdKey();
        commonDao.insert("product.insert", product);
    };
    
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void update(Product product) throws DataAccessException {
        // TODO 공구 진행 중일 경우 가격변동 불가
        
        
        Product existProduct = select(product.getPrdKey());
        // 가격 & 수량 변경
        if (existProduct.isPriceChanged(product)
                && existProduct.isPrdCntChanged(product)) {
            product.setPrdSellCnt(0);
            product.setPrdCancelCnt(0);
        } else if (existProduct.isPriceChanged(product)) { // 가격 변경
            int newPrdCnt = existProduct.getPrdCnt() - existProduct.getPrdSellCnt() + existProduct.getPrdCancelCnt();
            product.setPrdCnt(newPrdCnt);
            product.setPrdSellCnt(0);
            product.setPrdCancelCnt(0);
        } else if (existProduct.isPrdCntChanged(product)) { // 수량 변경
            product.setPrdSellCnt(existProduct.getPrdSellCnt());
            product.setPrdCancelCnt(existProduct.getPrdCancelCnt());
        }
        
        delete(product);
        insert(product);
    }
    
    @Override
    public void buy(Product product) throws DataAccessException {
        int stock = getStockCnt(product.getPrdKey());
        if (product.getPrdSellCnt() > stock) {
            throw new IllegalStateException("제품 수량이 부족합니다.");
        }
        
        commonDao.update("product.buy", product);
    }
    
    @Override
    public int getStockCnt(String prdKey) throws DataAccessException {
        return (int) commonDao.selectOne("product.getStockCnt", prdKey);
    };
    
    @Override
    public Category selectCategory(String prdCatCd) throws DataAccessException {
        return (Category) commonDao.selectOne("product.selectCategory", prdCatCd);
    };
    
    @Override
    public List<Category> selectCategoryList(boolean isAvail) throws DataAccessException {
        return commonDao.selectList("product.selectCategoryList", isAvail);
    };
    
    @Override
    public void deleteCategory(Category category) throws DataAccessException {
        commonDao.update("product.deleteCategory", category);
    };
    
    @Override
    public void insertCategory(Category category) throws DataAccessException {
        Optional<String> maybeMaxCatCd = commonDao.selectOptionalOne("product.selectMaxCatCd", null);
        category.makeCatCd(maybeMaxCatCd);
        commonDao.insert("product.insertCategory", category);
    };
    
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void updateCategory(Category category) throws DataAccessException {
        deleteCategory(category);
        commonDao.insert("product.insertCategory", category);
    }
}
