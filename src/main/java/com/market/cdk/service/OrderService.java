package com.market.cdk.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.market.cdk.bean.Customer;
import com.market.cdk.bean.Order;
import com.market.cdk.bean.Product;

public interface OrderService {
    void order(Customer customer, List<Product> productList, int ggKey) throws DataAccessException;
    
    List<Order> selectList(int ggKey, boolean isAvail) throws DataAccessException;
}
