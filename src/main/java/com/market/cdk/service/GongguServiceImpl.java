package com.market.cdk.service;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.market.cdk.bean.Gonggu;
import com.market.cdk.dao.CommonDao;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GongguServiceImpl implements GongguService {
    @Autowired
    CommonDao commonDao;
    
    @Override
    public Optional<Integer> getAvailGonggu() throws DataAccessException {
        return commonDao.selectOptionalOne("gonggu.selectAvail",null);
    }
    
    @Override
    public List<Gonggu> selectList(boolean isAvail) throws DataAccessException {
        return commonDao.selectList("gonggu.selectList", isAvail);
    }

    @Override
    public void insert(Gonggu gonggu) throws DataAccessException {
        commonDao.insert("gonggu.insert", gonggu);
    }

    @Override
    public void delete(Gonggu gonggu) throws DataAccessException {
        commonDao.insert("gonggu.delete", gonggu);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void update(Gonggu gonggu) throws DataAccessException {
        delete(gonggu);
        commonDao.insert("gonggu.insert", gonggu);
    }
}
