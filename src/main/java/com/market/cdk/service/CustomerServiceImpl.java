package com.market.cdk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.market.cdk.bean.Customer;
import com.market.cdk.dao.CommonDao;

@Service
@SuppressWarnings("unchecked")
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CommonDao commonDao;
    
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public Customer getCustomerForOrder(Customer customer) throws DataAccessException {
        // 기존 고객 정보 조회
        Customer existCustomer = select(customer);
        
        if (existCustomer == null) { // 없으면 insert
            insert(customer);
            existCustomer = select(customer);
        } else if (!existCustomer.equal(customer)) { // 일치하지 않으면 update
            delete(existCustomer);
            customer.setCreator("system");
            insert(customer);
            
            // 주문 데이터 입력을 위해 고객 정보를 새로 가져온다.
            existCustomer = select(customer);
        }
        
        return existCustomer;
    };
    
    @Override
    public Customer select(Customer customer) throws DataAccessException {
        return (Customer) commonDao.selectOne("customer.select", customer);
    }
    
    @Override
    public List<Customer> selectList(boolean isAvail) throws DataAccessException {
        return commonDao.selectList("customer.selectList", isAvail);
    }
    
    @Override
    public void insert(Customer customer) throws DataAccessException {
        commonDao.insert("customer.insert", customer);
    }
    
    @Override
    public void delete(Customer customer) throws DataAccessException {
        commonDao.update("customer.delete", customer);
    }
    
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void update(Customer customer) throws DataAccessException {
        delete(customer);
        insert(customer);
    }
}
