package com.market.cdk.service;

import com.market.cdk.bean.Gonggu;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Optional;

public interface GongguService {
    Optional<Integer> getAvailGonggu() throws DataAccessException;
    
    List<Gonggu> selectList(boolean isAvail) throws DataAccessException;

    void insert(Gonggu gonggu) throws DataAccessException;

    void delete(Gonggu gonggu) throws DataAccessException;

    void update(Gonggu gonggu) throws DataAccessException;
}
