package com.market.cdk.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.market.cdk.bean.Category;
import com.market.cdk.bean.Product;

public interface ProductService {
    Product select(String prdKey) throws DataAccessException;
    
    List<Product> selectList(boolean isAvail) throws DataAccessException;
    
    void delete(Product product) throws DataAccessException;
    
    void insert(Product product) throws DataAccessException;
    
    void update(Product product) throws DataAccessException;
    
    void buy(Product product) throws DataAccessException;
    
    int getStockCnt(String prdKey) throws DataAccessException;
    
    Category selectCategory(String prdCatCd) throws DataAccessException;
    
    List<Category> selectCategoryList(boolean isAvail) throws DataAccessException;
    
    void deleteCategory(Category category) throws DataAccessException;
    
    void insertCategory(Category category) throws DataAccessException;
    
    void updateCategory(Category category) throws DataAccessException;
}
