package com.market.cdk.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.market.cdk.bean.Customer;
import com.market.cdk.bean.Order;
import com.market.cdk.bean.Product;
import com.market.cdk.dao.CommonDao;
import com.market.cdk.util.CommonUtil;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
@SuppressWarnings("unchecked")
public class OrderServiceImpl implements OrderService {
    @Autowired
    CommonDao commonDao;
    
    @Autowired
    ProductService productService;
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    GongguService gongguService;
    
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void order(Customer customer, List<Product> productList, int ggKey) throws DataAccessException {
        // 고객 정보 insert, update, select
        Customer existCustomer = customerService.getCustomerForOrder(customer);
        
        // 고객 키 가져오기
        final int custKey = existCustomer.getCustKey();

        // 주문 메인 입력
        Order order = Order.builder()
                .ggKey(ggKey)
                .custKey(custKey)
                .creator("client")
                .build();
        commonDao.insert("order.insert", order);

        // 주문 키(필수) 가져오기
        int orderKey = commonDao.selectInt("order.selectKey", order);
        order.setOrderKey(orderKey);

        // 주문 상태 입력
        order.setOrderStatus(Order.Status.START.getCode());
        commonDao.insert("order.insertStat", order);

        // 제품 구매
        // TODO 제품 개수 0 확인
        productList.stream().forEach(product -> {
            // 재고 정보 업데이트
            productService.buy(product);

            // 주문 상세 입력
            order.setPrdKey(product.getPrdKey());
            order.setOrderCnt(product.getPrdSellCnt());
            commonDao.insert("order.insertDetail", order);
        });
    }

    @Override
    public List<Order> selectList(int ggKey, boolean isAvail) throws DataAccessException {
        Map params = new HashMap();
        params.put("ggKey", ggKey);
        params.put("isAvail", isAvail);
        return commonDao.selectList("order.selectList", params);
    }
}
