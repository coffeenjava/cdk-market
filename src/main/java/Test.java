import java.util.Optional;
import java.util.OptionalInt;
import java.util.Properties;

/**
 * Created by coffeenjava on 2017. 2. 4..
 */
public class Test {
    public static void main(String[] args) {
        Test t = new Test();
        t.optionalTest();
    }

    void optionalTest() {
        Optional<String> mayStr = Optional.of("hello");

        System.out.println(mayStr.map(s -> s));
    }

    int readDura(Properties prop, String name) {
        Optional<Object> mayStr = Optional.ofNullable(prop.get(name));
        mayStr.map(s -> {
            try {
                int i = Integer.parseInt((String)s);
                return  i > 0 ? i : 0;
            } catch (NumberFormatException ne) {}
            return 0;
        });
        return 0;
    }
}
